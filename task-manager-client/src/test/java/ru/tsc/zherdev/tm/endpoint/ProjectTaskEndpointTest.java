package ru.tsc.zherdev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.endpoint.*;

import java.util.Collections;
import java.util.List;

public class ProjectTaskEndpointTest {

    @NotNull
    private ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    private ProjectEndpoint projectEndpoint;

    @NotNull
    private TaskEndpoint taskEndpoint;

    @Nullable
    private Session session;

    @Nullable
    private Project project;

    @Nullable
    private Task task;
    
    @Before
    public void before() throws SQLException_Exception {
        final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();
        final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();

        session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(session);
        project = projectEndpoint.createProject(session, "test Project", "test Project description");
        Assert.assertNotNull(project);
        project.setUserId(session.getUserId());
        projectEndpoint.addProject(session, project);
        task = taskEndpoint.createTask(session, "test Task", "test Task description");
        Assert.assertNotNull(task);
        task.setUserId(session.getUserId());
        task.setProjectId(project.getId());
        taskEndpoint.addTask(session, task);
    }
    
    @Test
    public void unbindTaskById() throws SQLException_Exception {
        projectTaskEndpoint.unbindTaskById(session, project.getId(), task.getId());
        Assert.assertNull(taskEndpoint.findByIdTask(session, task.getId()).getProjectId());
    }

    @Test
    public void bindTaskById() throws SQLException_Exception {
        @Nullable Project testProject = projectEndpoint.createProject(session, "test2", "test2");
        Assert.assertNotNull(testProject);
        projectEndpoint.addProject(session, testProject);
        projectTaskEndpoint.bindTaskById(session, testProject.getId(), task.getId());

        Assert.assertNotNull(taskEndpoint.findByIdTask(session, task.getId()).getProjectId());
        projectTaskEndpoint.bindTaskById(session, project.getId(), task.getId());
    }

    @Test
    public void findTaskByProjectId() throws SQLException_Exception {
        final List<Task> tasks = projectTaskEndpoint.findTaskByProjectId(session, project.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, tasks);
        Task testTask = tasks.stream().filter(o -> o.getId() == task.getId())
                .findFirst()
                .get();
        Assert.assertEquals(task.getId(), testTask.getId());
    }

    @Test
    public void removeProjectById() throws SQLException_Exception {
        projectTaskEndpoint.removeProjectById(session, project.getId());
        Assert.assertNull(projectEndpoint.findByIdProject(session, project.getId()));
        Assert.assertNull(taskEndpoint.findByIdTask(session, task.getId()));
    }

}
