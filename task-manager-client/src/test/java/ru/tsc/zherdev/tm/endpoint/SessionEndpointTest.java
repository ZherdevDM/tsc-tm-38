package ru.tsc.zherdev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.SessionEndpoint;
import ru.tsc.zherdev.tm.endpoint.SessionEndpointService;
import ru.tsc.zherdev.tm.marker.SOAPCategory;

public class SessionEndpointTest {

    @NotNull
    private SessionEndpoint sessionEndpoint;

    @NotNull
    private Session session;

    @NotNull
    private Session invalidSession;

    @Before
    public void before() throws SQLException_Exception {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

        session = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionEndpoint.isValidSession(session));
        @Nullable final Session invalidSession = new Session();
        Assert.assertNotNull(invalidSession);
    }

    @Test
    @Category(SOAPCategory.class)
    public void isValidSession() {
        Assert.assertFalse(sessionEndpoint.isValidSession(invalidSession));
    }

    @Test
    @Category(SOAPCategory.class)
    public void closeSession() {
        Assert.assertTrue(sessionEndpoint.closeSession(session).isSuccess());
    }

    @Test
    @Category(SOAPCategory.class)
    public void openSession() throws SQLException_Exception {
        Assert.assertEquals(session.getUserId(), sessionEndpoint.openSession("admin", "admin").getUserId());
        final Session testSession = sessionEndpoint.openSession("invalidSession", "password");
        Assert.assertNull(testSession);
    }

}
