package ru.tsc.zherdev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.marker.SOAPCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private TaskEndpoint taskEndpoint;

    @NotNull
    private static final String TASK_NAME = "TaskTestName";

    @NotNull
    private static final String TASK_DESCRIPTION = "TaskTestDescription";

    @NotNull
    private static final String TASK_TEMP_NAME = "TaskTestTempName";

    @NotNull
    private static final String TASK_TEMP_DESCRIPTION = "TaskTestTempDescription";

    @NotNull
    private Task task;

    @NotNull
    private String taskId;

    @NotNull
    private Task taskTemp;

    @NotNull
    private String taskTempId;

    @NotNull
    private Session session;

    public TaskEndpointTest() {
    }

    @Before
    public void before() throws AbstractException, SQLException_Exception {
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        @NotNull SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

        session = sessionEndpoint.openSession("admin", "admin");
        task = taskEndpoint.createTask(session, TASK_NAME, TASK_DESCRIPTION);
        taskTemp = taskEndpoint.createTask(session, TASK_TEMP_NAME, TASK_TEMP_DESCRIPTION);
        taskId = task.getId();
        taskTempId = taskTemp.getId();
        taskEndpoint.addTask(session, task);
    }

    @Test
    @Category(SOAPCategory.class)
    public void createTask() throws AbstractException, SQLException_Exception {
        @Nullable final Task testTask = taskEndpoint.createTask(session, TASK_NAME, TASK_DESCRIPTION);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(task.getName(), testTask.getName());
        Assert.assertEquals(task.getDescription(), testTask.getDescription());
        Assert.assertEquals(task.getUserId(), testTask.getUserId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void addTask() throws AbstractException, SQLException_Exception {
        Assert.assertNotNull(taskEndpoint.addTask(session, taskTemp));
        @Nullable final Task testTask = taskEndpoint.findByIdTask(session, taskTempId);
        Assert.assertNotNull(testTask);
        Assert.assertEquals(taskTemp.getId(), testTask.getId());
        Assert.assertEquals(taskTemp.getName(), testTask.getName());
        Assert.assertEquals(taskTemp.getDescription(), testTask.getDescription());
        Assert.assertEquals(taskTemp.getUserId(), testTask.getUserId());
        taskEndpoint.removeUserTask(session, testTask);

        @Nullable final Task testTask2 = taskEndpoint.createTask(session, "Test2", "Test2");
        Assert.assertNotNull(testTask2);
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(taskTemp);
        tasks.add(testTask2);
        final Result result = new Result();
        result.setSuccess(true);
        Assert.assertEquals(taskEndpoint.addAllTask(session, tasks).isSuccess(), result.isSuccess());
        taskEndpoint.removeByIdTask(session, taskTempId);
        taskEndpoint.removeByIdTask(session, testTask2.getId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void findMethodsTest() throws AbstractException, SQLException_Exception {
        Assert.assertNotEquals(Collections.EMPTY_LIST, taskEndpoint.findAllTask(session));

        Assert.assertNotNull(taskEndpoint.findByIdTask(session, taskId));
        Assert.assertNotNull(taskEndpoint.findByNameTask(session, TASK_NAME));
        Assert.assertNotNull(taskEndpoint.findByIndexTask(session, 0));
    }


    @Test
    @Category(SOAPCategory.class)
    public void statusMethodsTest() throws AbstractException, SQLException_Exception {
        @Nullable Task startedTask = taskEndpoint.startByIdTask(session, task.getId());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());

        @Nullable Task finishedTask = taskEndpoint.finishByIdTask(session, task.getId());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());

        @Nullable Task statusChangedTask = taskEndpoint.changeStatusByIdTask(session, task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedTask.getStatus());

        startedTask = taskEndpoint.startByNameTask(session, task.getName());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());

        finishedTask = taskEndpoint.finishByNameTask(session, task.getName());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());

        statusChangedTask = taskEndpoint.changeStatusByNameTask(session, task.getName(), Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedTask.getStatus());

        startedTask = taskEndpoint.startByIndexTask(session, 0);
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());

        finishedTask = taskEndpoint.finishByIndexTask(session, 0);
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());

        statusChangedTask = taskEndpoint.changeStatusByIndexTask(session, 0, Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedTask.getStatus());
    }

    @Test
    @Category(SOAPCategory.class)
    public void removeMethodsTest() throws SQLException_Exception {
        @Nullable Task removableTask = createTestTask("remove me");
        taskEndpoint.removeUserTask(session, task);
        removableTask = taskEndpoint.findByIdTask(session, task.getId());
        Assert.assertNull(removableTask);
        removableTask = createTestTask("remove me by id");
        taskEndpoint.removeByIdTask(session, removableTask.getId());
        Assert.assertNull(taskEndpoint.findByIdTask(session, removableTask.getId()));
        removableTask = createTestTask("remove me by name");
        taskEndpoint.removeByNameTask(session, removableTask.getName());
        Assert.assertNull(taskEndpoint.findByIdTask(session, removableTask.getId()));
        removableTask = createTestTask("remove me by index");
        int tasksCount = taskEndpoint.findAllTask(session).size();
        if (tasksCount < 1) Assert.fail();
        taskEndpoint.removeByIndexTask(session, tasksCount - 1);
        Assert.assertNull(taskEndpoint.findByIdTask(session, removableTask.getId()));
        removableTask = createTestTask("remove us all");
        Assert.assertTrue(taskEndpoint.clearTask(session).isSuccess());
        List<Task> tasks = taskEndpoint.findAllUserTask(session);
        Assert.assertEquals(0, tasks.size());
    }

    @NotNull
    private Task createTestTask(@NotNull final String name) throws SQLException_Exception {
        @Nullable final Task testTask = taskEndpoint.createTask(session, name, "test me description");
        Assert.assertNotNull(testTask);
        testTask.setUserId(session.getUserId());
        taskEndpoint.addTask(session, testTask);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, testTask.getId()));
        return testTask;
    }

    @Test
    @Category(SOAPCategory.class)
    public void getSize() throws SQLException_Exception {
        Assert.assertNotEquals((Integer) 0, taskEndpoint.getSizeTask(session));
    }

    @Test
    @Category(SOAPCategory.class)
    public void existMethodsTest() throws SQLException_Exception {
        Assert.assertTrue(taskEndpoint.existsByIdTask(session, task.getId()));
        Assert.assertTrue(taskEndpoint.existsByIndexTask(session, 0));
    }

    @Test
    @Category(SOAPCategory.class)
    public void updateMethodsTest() throws SQLException_Exception {
        @NotNull Task testTask = createTestTask("update me by ID");
        taskEndpoint.updateByIdTask(session, testTask.getId(), "You updated me by ID", "And my description");
        @Nullable Task updatedTask = taskEndpoint.findByIdTask(session, testTask.getId());
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("You updated me by ID", updatedTask.getName());
        Assert.assertEquals("And my description", updatedTask.getDescription());

        testTask = createTestTask("update me by index");
        int tasksCount = taskEndpoint.findAllUserTask(session).size();
        if (tasksCount < 1) Assert.fail();
        taskEndpoint.updateByIndexTask(session, tasksCount - 1, "You updated me by index", "And my description");
        updatedTask = taskEndpoint.findByIndexTask(session, tasksCount - 1);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("You updated me by index", updatedTask.getName());
        Assert.assertEquals("And my description", updatedTask.getDescription());
    }

}
