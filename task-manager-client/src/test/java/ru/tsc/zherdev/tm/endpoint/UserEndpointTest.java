package ru.tsc.zherdev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.marker.SOAPCategory;

public class UserEndpointTest {

    @NotNull
    private UserEndpoint userEndpoint;

    @NotNull
    private User user;

    @NotNull
    private SessionEndpoint sessionEndpoint;

    @NotNull
    private Session session;

    @Before
    public void before() throws SQLException_Exception {
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();

        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

        session = sessionEndpoint.openSession("user", "user");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionEndpoint.isValidSession(session));
        user = userEndpoint.getUserSelfByUserId(session);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(SOAPCategory.class)
    public void setPasswordUser() throws SQLException_Exception {
        userEndpoint.setPasswordUser(session, "changed");
        Assert.assertEquals(session.getUserId(), sessionEndpoint.openSession("user", "changed").getUserId());
        userEndpoint.setPasswordUser(session, "user");
    }

    @Test
    @Category(SOAPCategory.class)
    public void getSelfUserId() throws SQLException_Exception {
        Assert.assertEquals(user.getId(), userEndpoint.getUserSelfByUserId(session).getId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void updateUserById() throws SQLException_Exception {
        @Nullable final User changedUser = userEndpoint.updateUserByUserId(
                session, "user", "email@m.com", "Lee", "Jet", "ddd"
        );
        Assert.assertNotNull(changedUser);
        Assert.assertEquals("email@m.com", changedUser.getEmail());
        Assert.assertEquals("Lee", changedUser.getLastName());
        Assert.assertEquals("Jet", changedUser.getFirstName());
        Assert.assertEquals("ddd", changedUser.getMiddleName());
        Assert.assertEquals(user.getId(), changedUser.getId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void updateUserByLogin() throws SQLException_Exception {
        @Nullable final User changedUser = userEndpoint.updateUserByUserLogin(
                session, "user", "user", "email@m.com", "Lee", "Jet", "ddd"
        );
        Assert.assertNotNull(changedUser);
        Assert.assertEquals("email@m.com", changedUser.getEmail());
        Assert.assertEquals("Lee", changedUser.getLastName());
        Assert.assertEquals("Jet", changedUser.getFirstName());
        Assert.assertEquals("ddd", changedUser.getMiddleName());
        Assert.assertEquals(user.getId(), changedUser.getId());
    }

}
