package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.marker.SOAPCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public class ProjectEndpointTest {

    @NotNull
    private ProjectEndpoint projectEndpoint;

    @NotNull
    private static final String PROJECT_NAME = "ProjectTestName";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "ProjectTestDescription";

    @NotNull
    private static final String PROJECT_TEMP_NAME = "ProjectTestTempName";

    @NotNull
    private static final String PROJECT_TEMP_DESCRIPTION = "ProjectTestTempDescription";

    @NotNull
    private Project project;

    @NotNull
    private String projectId;

    @NotNull
    private Project projectTemp;

    @NotNull
    private String projectTempId;

    @NotNull
    private Session session;

    @Before
    public void before() throws AbstractException, SQLException_Exception {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        @NotNull SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

        session = sessionEndpoint.openSession("admin", "admin");
        project = projectEndpoint.createProject(session, PROJECT_NAME, PROJECT_DESCRIPTION);
        projectTemp = projectEndpoint.createProject(session, PROJECT_TEMP_NAME, PROJECT_TEMP_DESCRIPTION);
        projectId = project.getId();
        projectTempId = projectTemp.getId();
        projectEndpoint.addProject(session, project);
    }

    @Test
    @Category(SOAPCategory.class)
    public void createProject() throws AbstractException, SQLException_Exception {
        @Nullable final Project testProject = projectEndpoint.createProject(session, PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertNotNull(testProject);
        Assert.assertEquals(project.getName(), testProject.getName());
        Assert.assertEquals(project.getDescription(), testProject.getDescription());
        Assert.assertEquals(project.getUserId(), testProject.getUserId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void addProject() throws AbstractException, SQLException_Exception {
        Assert.assertNotNull(projectEndpoint.addProject(session, projectTemp));
        @Nullable final Project testProject = projectEndpoint.findByIdProject(session, projectTempId);
        Assert.assertNotNull(testProject);
        Assert.assertEquals(projectTemp.getId(), testProject.getId());
        Assert.assertEquals(projectTemp.getName(), testProject.getName());
        Assert.assertEquals(projectTemp.getDescription(), testProject.getDescription());
        Assert.assertEquals(projectTemp.getUserId(), testProject.getUserId());
        projectEndpoint.removeProject(session, testProject);

        @Nullable final Project testProject2 = projectEndpoint.createProject(session, "Test2", "Test2");
        Assert.assertNotNull(testProject2);
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(projectTemp);
        projects.add(testProject2);
        final Result result = new Result();
        result.setSuccess(true);
        Assert.assertEquals(projectEndpoint.addAllProject(session, projects).isSuccess(), result.isSuccess());
        projectEndpoint.removeByIdProject(session, projectTempId);
        projectEndpoint.removeByIdProject(session, testProject2.getId());
    }

    @Test
    @Category(SOAPCategory.class)
    public void findMethodsTest() throws AbstractException, SQLException_Exception {
        Assert.assertNotEquals(Collections.EMPTY_LIST, projectEndpoint.findAllProject(session));

        Assert.assertNotNull(projectEndpoint.findByIdProject(session, projectId));
        Assert.assertNotNull(projectEndpoint.findByNameProject(session, PROJECT_NAME));
        Assert.assertNotNull(projectEndpoint.findByIndexProject(session, 0));
    }


    @Test
    @Category(SOAPCategory.class)
    public void statusMethodsTest() throws AbstractException, SQLException_Exception {
        @Nullable Project startedProject = projectEndpoint.startByIdProject(session, project.getId());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());

        @Nullable Project finishedProject = projectEndpoint.finishByIdProject(session, project.getId());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());

        @Nullable Project statusChangedProject = projectEndpoint.changeStatusByIdProject(session, project.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedProject.getStatus());

        startedProject = projectEndpoint.startByNameProject(session, project.getName());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());

        finishedProject = projectEndpoint.finishByNameProject(session, project.getName());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());

        statusChangedProject = projectEndpoint.changeStatusByNameProject(session, project.getName(), Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedProject.getStatus());

        startedProject = projectEndpoint.startByIndexProject(session, 0);
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());

        finishedProject = projectEndpoint.finishByIndexProject(session, 0);
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());

        statusChangedProject = projectEndpoint.changeStatusByIndexProject(session, 0, Status.NOT_STARTED);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.NOT_STARTED, statusChangedProject.getStatus());
    }

    @Test
    @Category(SOAPCategory.class)
    public void removeMethodsTest() throws SQLException_Exception {
        @Nullable Project removableProject = createTestProject("remove me");
        projectEndpoint.removeProject(session, project);
        removableProject = projectEndpoint.findByIdProject(session, project.getId());
        Assert.assertNull(removableProject);
        removableProject = createTestProject("remove me by id");
        projectEndpoint.removeByIdProject(session, removableProject.getId());
        Assert.assertNull(projectEndpoint.findByIdProject(session, removableProject.getId()));
        removableProject = createTestProject("remove me by name");
        projectEndpoint.removeByNameProject(session, removableProject.getName());
        Assert.assertNull(projectEndpoint.findByIdProject(session, removableProject.getId()));
        removableProject = createTestProject("remove me by index");
        int projectsCount = projectEndpoint.findAllProject(session).size();
        if (projectsCount < 1) Assert.fail();
        projectEndpoint.removeByIndexProject(session, projectsCount - 1);
        Assert.assertNull(projectEndpoint.findByIdProject(session, removableProject.getId()));
        removableProject = createTestProject("remove us all");
        Assert.assertTrue(projectEndpoint.clearProject(session).isSuccess());
        List<Project> projects = projectEndpoint.findAllUserProject(session);
        Assert.assertEquals(0, projects.size());
    }

    @NotNull
    private Project createTestProject(@NotNull final String name) throws SQLException_Exception {
        @Nullable final Project testProject = projectEndpoint.createProject(session, name, "test me description");
        Assert.assertNotNull(testProject);
        testProject.setUserId(session.getUserId());
        projectEndpoint.addProject(session, testProject);
        Assert.assertNotNull(projectEndpoint.findByIdProject(session, testProject.getId()));
        return testProject;
    }

    @Test
    @Category(SOAPCategory.class)
    public void getSize() throws SQLException_Exception {
        Assert.assertNotEquals((Integer) 0, projectEndpoint.getSizeProject(session));
    }

    @Test
    @Category(SOAPCategory.class)
    public void existMethodsTest() throws SQLException_Exception {
        Assert.assertTrue(projectEndpoint.existsByIdProject(session, project.getId()));
        Assert.assertTrue(projectEndpoint.existsByIndexProject(session, 0));
    }

    @Test
    @Category(SOAPCategory.class)
    public void updateMethodsTest() throws SQLException_Exception {
        @NotNull Project testProject = createTestProject("update me by ID");
        projectEndpoint.updateByIdProject(session, testProject.getId(), "You updated me by ID", "And my description");
        @Nullable Project updatedProject = projectEndpoint.findByIdProject(session, testProject.getId());
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("You updated me by ID", updatedProject.getName());
        Assert.assertEquals("And my description", updatedProject.getDescription());

        testProject = createTestProject("update me by index");
        int projectsCount = projectEndpoint.findAllUserProject(session).size();
        if (projectsCount < 1) Assert.fail();
        projectEndpoint.updateByIndexProject(session, projectsCount - 1, "You updated me by index", "And my description");
        updatedProject = projectEndpoint.findByIndexProject(session, projectsCount - 1);
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("You updated me by index", updatedProject.getName());
        Assert.assertEquals("And my description", updatedProject.getDescription());
    }

}
