package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.service.ISessionService;
import ru.tsc.zherdev.tm.api.service.IToolsLocator;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.exception.entity.InvalidSessionException;

public class SessionService implements ISessionService {

    @NotNull
    private final IToolsLocator toolsLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final IToolsLocator toolsLocator,
            @NotNull final ISessionRepository sessionRepository
    ) {
        this.toolsLocator = toolsLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    public Session getSession() {
        if (toolsLocator.getSessionEndpoint().isValidSession(sessionRepository.getSession()))
            return sessionRepository.getSession();
        else throw new InvalidSessionException();
    }

    @Override
    @Nullable
    public Session setSession(@Nullable final Session session) {
        return sessionRepository.setSession(session);
    }

}
