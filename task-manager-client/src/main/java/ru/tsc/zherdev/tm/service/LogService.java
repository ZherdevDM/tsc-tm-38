package ru.tsc.zherdev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

@NoArgsConstructor
public final class LogService implements ILogService {

    private static final String FILE_NAME = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";
    private static final String COMMANDS_FILE = "./commands.txt";

    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGES_FILE = "./messages.txt";

    private static final String ERRORS = "ERRORS";
    private static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();
    @NotNull
    private final LogManager manager = LogManager.getLogManager();
    @NotNull
    private final Logger root = Logger.getLogger("");
    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);
    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
        registry(errors, ERRORS_FILE, true);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            final InputStream inputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
