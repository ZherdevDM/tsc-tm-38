package ru.tsc.zherdev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

public final class ExitCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.EXIT;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }


}
