package ru.tsc.zherdev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.Sort;
import ru.tsc.zherdev.tm.endpoint.Task;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowByIdWithTasksCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.PROJECT_SHOW_BY_ID_WITH_TASKS;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by id with related tasks.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (!toolsLocator.getProjectEndpoint().existsByIdProject(session, projectId)) {
            throw new ProjectNotFoundException();
        }
        System.out.println("[PROJECT NAME:" +
                toolsLocator.getProjectEndpoint().findByIdProject(session, projectId).getName() + "]");
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> taskList;
        try {
            if (sort == null || sort.isEmpty())
                taskList = toolsLocator.getProjectTaskEndpoint().findTaskByProjectId(session, projectId);
            else {
                @NotNull final Sort sortType = Sort.valueOf(sort);
                System.out.println(sortType.value());
                taskList = toolsLocator.getProjectTaskEndpoint().findTaskByProjectId(session, projectId);
            }
        } catch (IllegalArgumentException e) {

            throw new SortIncorrectException();
        }
        for (Task task : taskList) {
            System.out.println(task);
        }
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}
