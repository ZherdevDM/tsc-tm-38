package ru.tsc.zherdev.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(String message, Throwable cause) {
        super(message, cause);
    }

}
