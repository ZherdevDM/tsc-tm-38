package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.Session;

public interface ISessionService {
    Session getSession();

    @Nullable Session setSession(@Nullable Session session);
}
