package ru.tsc.zherdev.tm.constant;

public final class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String INFO = "info";

    public static final String EXIT = "exit";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_SHOW_BY_ID = "task-show-by-id";

    public static final String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public static final String TASK_SHOW_BY_NAME = "task-show-by-name";

    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String TASK_START_BY_NAME = "task-start-by-name";

    public static final String TASK_START_BY_ID = "task-start-by-id";

    public static final String TASK_START_BY_INDEX = "task-start-by-index";

    public static final String TASK_FINISH_BY_NAME = "task-finish-by-name";

    public static final String TASK_FINISH_BY_ID = "task-finish-by-id";

    public static final String TASK_FINISH_BY_INDEX = "task-finish-by-index";

    public static final String TASK_CHANGE_STATUS_BY_NAME = "task-change-status-by-name";

    public static final String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public static final String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public static final String TASK_BIND_TO_PROJECT_BY_ID = "task-bind-to-project-by-id";

    public static final String TASK_UNBIND_TO_PROJECT_BY_ID = "task-unbind-to-project-by-id";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public static final String PROJECT_SHOW_BY_ID_WITH_TASKS = "project-show-by-id-with-tasks";

    public static final String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public static final String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String PROJECT_START_BY_NAME = "project-start-by-name";

    public static final String PROJECT_START_BY_ID = "project-start-by-id";

    public static final String PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String PROJECT_FINISH_BY_NAME = "project-finish-by-name";

    public static final String PROJECT_FINISH_BY_ID = "project-finish-by-id";

    public static final String PROJECT_FINISH_BY_INDEX = "project-finish-by-index";

    public static final String PROJECT_CHANGE_STATUS_BY_NAME = "project-change-status-by-name";

    public static final String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public static final String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public static final String LOGIN = "login";

    public static final String LOGOUT = "logout";

    public static final String USER_LIST = "user-list";

    public static final String SIGN_UP = "sign-up";

    public static final String USER_CLEAR = "user-clear";

    public static final String USER_CHANGE_PASSWORD = "user-change-password";

    public static final String USER_CHANGE_ROLE = "user-change-role";

    public static final String USER_REMOVE_BY_ID = "user-remove-by-id";

    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";

    public static final String USER_SHOW_BY_ID = "user-show-by-id";

    public static final String USER_SHOW_BY_LOGIN = "user-show-by-login";

    public static final String USER_UPDATE_BY_ID = "user-update-by-id";

    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";

}
