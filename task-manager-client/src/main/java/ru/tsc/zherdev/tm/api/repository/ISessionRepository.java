package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.Session;

public interface ISessionRepository {

    @Nullable
    Session getSession();

    @Nullable
    Session setSession(@Nullable final Session session);

}
