package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.exception.user.UserExistsWithLoginException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.USER_CHANGE_PASSWORD;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        if (!toolsLocator.getSessionEndpoint().isValidSession(session)) throw new AccessDeniedException();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = toolsLocator.getUserEndpoint().getUserSelfByUserId(session);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        if (!user.getLogin().equals(login)) throw new UserExistsWithLoginException(login);
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final User userChange = toolsLocator.getUserEndpoint().setPasswordUser(session, password);
        Optional.ofNullable(userChange).orElseThrow(UserNotFoundException::new);
        showUser(userChange);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}