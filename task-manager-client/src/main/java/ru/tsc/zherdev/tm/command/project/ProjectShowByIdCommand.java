package ru.tsc.zherdev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Project;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.exception.empty.EmptyProjectIdException;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectShowByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.PROJECT_SHOW_BY_ID;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws SQLException_Exception {
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        Optional.ofNullable(id).orElseThrow(EmptyProjectIdException::new);
        @Nullable final Project project = toolsLocator.getProjectEndpoint().findByIdProject(session, id);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    public void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}