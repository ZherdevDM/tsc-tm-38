package ru.tsc.zherdev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.IToolsLocator;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;

public abstract class AbstractCommand {

    protected IToolsLocator toolsLocator;

    public void setToolsLocator(IToolsLocator toolsLocator) {
        this.toolsLocator = toolsLocator;
    }

    public @Nullable Role[] roles() {
        return null;
    }

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String description();


    public abstract void execute() throws SQLException_Exception;

    @Override
    public String toString() {
        String result = "";
        final String name = name();
        final String arg = arg();
        final String description = description();

        if (!name.isEmpty()) result += name;
        if (arg != null) if (!arg.isEmpty()) result += " [" + arg + "]";
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

}
