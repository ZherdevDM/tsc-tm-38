package ru.tsc.zherdev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

import java.util.HashMap;

public final class ArgumentDisplayCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final HashMap<String, AbstractCommand> arguments = toolsLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments.values()) showCommandValue(argument.arg());
    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        final String bullet = "• ";
        System.out.println(bullet + value);
    }
}
