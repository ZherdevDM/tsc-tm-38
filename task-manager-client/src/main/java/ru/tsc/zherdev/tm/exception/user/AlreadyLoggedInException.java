package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class AlreadyLoggedInException extends AbstractException {

    public AlreadyLoggedInException() {
        super("You have already logged in.");
    }

}
