package ru.tsc.zherdev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

import java.util.Collection;

public final class HelpDisplayCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.HELP;
    }

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @Nullable final Collection<AbstractCommand> commands;
        commands = toolsLocator.getCommandService().getCommands().values();
        commands.forEach(System.out::println);
    }
}