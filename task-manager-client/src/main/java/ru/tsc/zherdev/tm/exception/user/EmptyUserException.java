package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error. User is empty.");
    }

}
