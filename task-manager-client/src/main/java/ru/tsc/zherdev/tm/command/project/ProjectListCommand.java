package ru.tsc.zherdev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Project;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.Sort;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[SHOW PROJECTS]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projectList;
        try {
            if (sort.isEmpty() || sort.equals("\\"))
                projectList = toolsLocator.getProjectEndpoint().findAllUserProject(session);
            else {
                projectList = toolsLocator.getProjectEndpoint().findAllUserProjectSorted(session, sort);
            }
        } catch (Exception e) {
            projectList = Collections.emptyList();
            System.out.println("WRONG SORT TYPE");
            throw new SortIncorrectException();
        }

        final String separator = "• ";
        for (Project project : projectList) System.out.println(separator + project.toString());
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}