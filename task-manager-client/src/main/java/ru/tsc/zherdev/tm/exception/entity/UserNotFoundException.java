package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
