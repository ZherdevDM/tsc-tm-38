package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.HashMap;

public interface ICommandService {

    @NotNull
    HashMap<String, AbstractCommand> getCommands();

    @NotNull
    HashMap<String, AbstractCommand> getArguments();

    @NotNull
    AbstractCommand getCommandByName(final String name);

    @NotNull
    AbstractCommand getCommandByArg(final String arg);

    @NotNull
    Collection<String> getListCommandName();

    @NotNull
    Collection<String> getListCommandArg();

    void add(final AbstractCommand command);

}
