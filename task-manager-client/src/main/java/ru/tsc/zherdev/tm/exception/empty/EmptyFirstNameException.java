package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error. First Name is empty.");
    }

}
