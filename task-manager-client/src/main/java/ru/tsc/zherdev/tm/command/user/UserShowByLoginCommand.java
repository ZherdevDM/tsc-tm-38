package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserShowByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.USER_SHOW_BY_LOGIN;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Show user profile by login.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[SHOW USER]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = toolsLocator.getAdminUserEndpoint().findUserByLoginUser(session, login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        if (!session.getUserId().equals(user.getId()) ||
                !user.getRole().equals(Role.ADMIN))
            throw new AccessDeniedException();
        showUser(user);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}
