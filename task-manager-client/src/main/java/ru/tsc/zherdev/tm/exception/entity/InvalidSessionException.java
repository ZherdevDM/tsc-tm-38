package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class InvalidSessionException extends AbstractException {

    public InvalidSessionException() {
        super("Error. Session is invalid. Please authorize again.");
    }

}
