package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserUpdateProfileByIdCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.USER_UPDATE_BY_ID;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Update user profile by id.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        if (!toolsLocator.getSessionEndpoint().isValidSession(session)) throw new AccessDeniedException();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final User user = toolsLocator.getAdminUserEndpoint().findUserByIdUser(session, id);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter Last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter First name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final User userChange =
                toolsLocator.getUserEndpoint().updateUserByUserId(session, password, lastName,
                        firstName, middleName, email);
        Optional.ofNullable(userChange).orElseThrow(UserNotFoundException::new);
        showUser(userChange);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
