package ru.tsc.zherdev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.Task;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

public final class TaskBindToProjectByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.TASK_BIND_TO_PROJECT_BY_ID;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (!toolsLocator.getTaskEndpoint().existsByIdTask(session, taskId)) {
            throw new TaskNotFoundException();
        }
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (!toolsLocator.getProjectEndpoint().existsByIdProject(session, projectId)) {
            throw new ProjectNotFoundException();
        }
        @Nullable final Task task = toolsLocator.getProjectTaskEndpoint().bindTaskById(session, projectId, taskId);
        System.out.println("[PROJECT NAME:" + toolsLocator.getProjectEndpoint().findByIdProject(session, projectId).getName() + "]");
        System.out.println("[TASK NAME:" + task.getName() + " BINDING FINISHED]");
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}