package ru.tsc.zherdev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.exception.entity.InvalidSessionException;

@NoArgsConstructor
public class SessionRepository implements ISessionRepository {

    @Nullable
    private Session session = null;

    @Override
    public Session getSession() {
        return session;
    }

    @Nullable
    @Override
    public Session setSession(@Nullable final Session session) {
        this.session = session;
        return session;
    }

}
