package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }

}
