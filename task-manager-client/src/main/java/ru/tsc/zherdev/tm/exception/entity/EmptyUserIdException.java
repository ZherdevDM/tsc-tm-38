package ru.tsc.zherdev.tm.exception.entity;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error. User ID is empty.");
    }

}
