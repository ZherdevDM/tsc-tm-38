package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ICommandRepository;
import ru.tsc.zherdev.tm.api.service.ICommandService;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.exception.empty.EmptyArgumentException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.system.UnknownCommandException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public final class CommandService implements ICommandService {

    final private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public HashMap<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public HashMap<String, AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByName(@Nullable String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @Nullable final AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new UnknownCommandException();
        return command;
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByArg(@Nullable String arg) {
        Optional.ofNullable(arg).orElseThrow(EmptyArgumentException::new);
        @Nullable final AbstractCommand command = commandRepository.getCommandByArg(arg);
        if (command == null) throw new UnknownCommandException();
        return command;
    }

    @Override
    @NotNull
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public Collection<String> getListCommandArg() {
        return commandRepository.getCommandArg();
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
