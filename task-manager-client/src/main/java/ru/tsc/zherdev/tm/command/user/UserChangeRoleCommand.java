package ru.tsc.zherdev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.User;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class UserChangeRoleCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.USER_CHANGE_ROLE;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Change user role.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[CHANGE USER ROLE]");
        System.out.println("Enter ID:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = toolsLocator.getAdminUserEndpoint().findByIdUser(session, userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("Enter role:");
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final String roleId = TerminalUtil.nextLine();
        @NotNull final Role role = Role.valueOf(roleId.toUpperCase());
        @Nullable final User userChange = toolsLocator.getAdminUserEndpoint().setRoleUser(session, role);
        Optional.ofNullable(userChange).orElseThrow(UserNotFoundException::new);
        showUser(userChange);
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
