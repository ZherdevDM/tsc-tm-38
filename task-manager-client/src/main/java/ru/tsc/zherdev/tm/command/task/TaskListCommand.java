package ru.tsc.zherdev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.endpoint.Sort;
import ru.tsc.zherdev.tm.endpoint.Task;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Display all tasks.";
    }

    @Override
    public void execute() throws SQLException_Exception{
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        System.out.println("[SHOW TASKS]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> taskList;
        try {
            if (sort == null || sort.isEmpty())
                taskList = toolsLocator.getTaskEndpoint().findAllTask(session);
            else {
                System.out.println(sort);
                taskList = toolsLocator.getTaskEndpoint().findAllUserTaskSorted(session, sort);
            }
        } catch (IllegalArgumentException e) {
            taskList = Collections.emptyList();
            throw new SortIncorrectException(sort);
        }

        int separator = 1;
        for (Task task : taskList) {
            System.out.println(separator + ". " + task.toString());
            separator++;
        }
    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}
