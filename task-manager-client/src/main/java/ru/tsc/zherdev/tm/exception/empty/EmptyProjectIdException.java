package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyProjectIdException extends AbstractException {
    public EmptyProjectIdException() {
        super("Error. Project ID is empty.");
    }
}
