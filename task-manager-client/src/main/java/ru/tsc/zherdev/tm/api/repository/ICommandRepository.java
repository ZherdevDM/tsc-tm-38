package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.HashMap;

public interface ICommandRepository {

    @NotNull
    HashMap<String, AbstractCommand> getCommands();

    @NotNull
    HashMap<String, AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @Nullable
    AbstractCommand getCommandByName(@NotNull final String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull final String arg);

    void add(@NotNull final AbstractCommand command);

}
