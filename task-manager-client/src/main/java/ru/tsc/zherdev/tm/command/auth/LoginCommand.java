package ru.tsc.zherdev.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.Session;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractAuthCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.LOGIN;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Login to task-manager.";
    }

    @Override
    public void execute() throws SQLException_Exception {
        System.out.println("[LOGIN]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable Session session = toolsLocator.getSessionEndpoint().openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        toolsLocator.getSessionService().setSession(session);
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserNotFoundException();
    }

}

