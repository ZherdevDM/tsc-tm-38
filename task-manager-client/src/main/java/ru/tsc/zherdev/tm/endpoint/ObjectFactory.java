
package ru.tsc.zherdev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tsc.zherdev.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SQLException_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "SQLException");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "closeSessionResponse");
    private final static QName _IsValidSession_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "isValidSession");
    private final static QName _IsValidSessionResponse_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "isValidSessionResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.zherdev.tsc.ru/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.zherdev.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SQLException }
     * 
     */
    public SQLException createSQLException() {
        return new SQLException();
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link IsValidSession }
     * 
     */
    public IsValidSession createIsValidSession() {
        return new IsValidSession();
    }

    /**
     * Create an instance of {@link IsValidSessionResponse }
     * 
     */
    public IsValidSessionResponse createIsValidSessionResponse() {
        return new IsValidSessionResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link SqlException2 }
     * 
     */
    public SqlException2 createSqlException2() {
        return new SqlException2();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "SQLException")
    public JAXBElement<SQLException> createSQLException(SQLException value) {
        return new JAXBElement<SQLException>(_SQLException_QNAME, SQLException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValidSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsValidSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "isValidSession")
    public JAXBElement<IsValidSession> createIsValidSession(IsValidSession value) {
        return new JAXBElement<IsValidSession>(_IsValidSession_QNAME, IsValidSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValidSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsValidSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "isValidSessionResponse")
    public JAXBElement<IsValidSessionResponse> createIsValidSessionResponse(IsValidSessionResponse value) {
        return new JAXBElement<IsValidSessionResponse>(_IsValidSessionResponse_QNAME, IsValidSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.zherdev.tsc.ru/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
