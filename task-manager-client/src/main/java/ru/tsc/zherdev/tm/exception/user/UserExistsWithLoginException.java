package ru.tsc.zherdev.tm.exception.user;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class UserExistsWithLoginException extends AbstractException {

    public UserExistsWithLoginException(final String login) {
        super("Error. Such user '" + login + "' already exists.");
    }

}
