package ru.tsc.zherdev.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.constant.TerminalConst;
import ru.tsc.zherdev.tm.endpoint.Role;
import ru.tsc.zherdev.tm.endpoint.SQLException_Exception;
import ru.tsc.zherdev.tm.endpoint.Session;

public final class LogOutCommand extends AbstractAuthCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.LOGOUT;
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() throws SQLException_Exception {
        System.out.println("[LOGOUT]");
        @Nullable final Session session = toolsLocator.getSessionService().getSession();
        toolsLocator.getSessionEndpoint().closeSession(session);

    }

    @Override
    public ru.tsc.zherdev.tm.endpoint.@Nullable Role[] roles() {
        return Role.values();
    }

}
