package ru.tsc.zherdev.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.command.AbstractCommand;
import ru.tsc.zherdev.tm.constant.TerminalConst;

public final class VersionDisplayCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return TerminalConst.VERSION;
    }

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[Version]");
        System.out.println(toolsLocator.getPropertyService().getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}