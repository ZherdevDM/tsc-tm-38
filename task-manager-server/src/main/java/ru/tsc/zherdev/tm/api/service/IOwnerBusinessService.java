package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

import java.sql.SQLException;

public interface IOwnerBusinessService<E extends AbstractOwnerBusinessEntity> extends IOwnerBusinessRepository<E> {

}
