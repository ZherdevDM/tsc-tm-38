package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;


public abstract class AbstractOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity> extends AbstractRepository<E>
        implements IOwnerBusinessRepository<E> {

    protected AbstractOwnerBusinessRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.execute();
        }
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @Nullable final E entity = findByName(userId, name);
        if (entity == null) throw new EntityNotFoundException();
        remove(entity);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @Nullable final E entity = findById(id);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setString(1, userId);
            statement.setString(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void removeByIndex(@NotNull String userId, @NotNull Integer index) throws SQLException {
        final E entity = findByIndex(index);
        if (entity == null) throw new EntityNotFoundException();
        removeById(userId, entity.getId());
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.executeQuery();
        }
    }

    @Override
    public @NotNull Integer getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT COUNT(1) FROM  \"task-manager\"" + getTableName() + " WHERE user_id = ?;";
        final int result;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            result = resultSet.getInt(1);
            if (!resultSet.next()) throw new EntityNotFoundException();
        }
        return result;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?;";
        @NotNull final List<E> result = new ArrayList<>();
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator) throws SQLException {
        if (comparator == null) return findAll(userId);
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @Nullable
    public E findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final E entity;
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, name);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new EntityNotFoundException();
            entity = fetch(resultSet);
        }
        return entity;
    }

    @Override
    @Nullable
    public E findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final E entity;
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new EntityNotFoundException();
            entity = fetch(resultSet);
        }
        return entity;

    }

    @Override
    @Nullable
    public E findByIndex(@NotNull final String userId, final @NotNull Integer index) throws SQLException {
        if (existsByIndex(userId, index)) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?;";
        @NotNull final E entity;
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new IndexIncorrectException();
            entity = fetch(resultSet);
        }
        return entity;

    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        return (index >= 0 && index < getSize(userId));
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        try {
            findById(userId, id);
            return true;
        } catch (AbstractException | SQLException exception) {
            return false;
        }
    }

    @Override
    @Nullable
    public E startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and name = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.IN_PROGRESS.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, name);
            statement.executeUpdate();
        }
        return findById(userId, name);
    }

    @Override
    @Nullable
    public E startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.IN_PROGRESS.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findById(userId, id);
    }

    @Override
    @Nullable
    public E startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @Nullable final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.IN_PROGRESS.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, entity.getId());
            statement.executeUpdate();
        }
        return findById(userId, entity.getId());
    }

    @Override
    @Nullable
    public E finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and name = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.COMPLETE.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, name);
            statement.executeUpdate();
        }
        return findById(userId, name);
    }

    @Override
    @Nullable
    public E finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.COMPLETE.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findById(userId, id);
    }

    @Override
    @Nullable
    public E finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @Nullable final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Status.COMPLETE.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, entity.getId());
            statement.executeUpdate();
        }
        return findById(userId, entity.getId());
    }

    @Override
    @Nullable
    public E updateById(@NotNull final String userId, @NotNull final String id,
                        @NotNull final String name, @NotNull final String description) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET name = ?, descption = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findById(userId, id);
    }

    @Override
    @Nullable
    public E updateByIndex(@NotNull final String userId, @NotNull final Integer index,
                           @NotNull final String name, @Nullable final String description) throws SQLException {
        @Nullable final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET name = ?, descption = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            if (description == null) statement.setNull(2, 12);
            else statement.setString(2, description);
            statement.setString(3, userId);
            statement.setString(4, entity.getId());
            statement.executeUpdate();
        }
        return findById(userId, entity.getId());
    }

    @NotNull
    private Optional<E> updateStatus(@NotNull final E entity, @NotNull final Status status) {
        final Optional<E> optional = Optional.of(entity);
        optional.ifPresent(o -> {
            o.setStatus(status);
            if (status.equals(Status.IN_PROGRESS)) o.setStartDate(new Date());
            else if (status.equals(Status.COMPLETE)) o.setFinishDate(new Date());
        });
        return optional;
    }

    @Override
    @Nullable
    public E changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and name = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, name);
            statement.executeUpdate();
        }
        return findById(userId, name);
    }

    @Override
    @Nullable
    public E changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findById(userId, id);
    }

    @Override
    @Nullable
    public E changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) throws SQLException {
        @Nullable final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET status = ?, start_date = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status.toString());
            statement.setDate(2, new java.sql.Date(new Date().getTime()));
            statement.setString(3, userId);
            statement.setString(4, entity.getId());
            statement.executeUpdate();
        }
        return findById(userId, entity.getId());
    }

}
