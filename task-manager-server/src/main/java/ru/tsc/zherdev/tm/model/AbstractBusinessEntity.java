package ru.tsc.zherdev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.entity.IWBS;
import ru.tsc.zherdev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date finishDate;

    @NotNull
    protected Date created = new Date();

}
