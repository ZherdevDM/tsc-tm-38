package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;

public interface IUserService extends IUserRepository {

    @NotNull
    User add(@Nullable final String login, @Nullable final String password) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException;

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException;

    @NotNull
    User setPassword(@Nullable final String userId, @Nullable final String password) throws SQLException;

    @Nullable
    User setRole(@Nullable final String userId, @Nullable final Role role) throws SQLException;

    @Nullable
    User findUserById(@Nullable final String id) throws SQLException;

    @Nullable User userValidate(@Nullable String login, @Nullable String password) throws SQLException;

    boolean isLoginExists(@Nullable final String login) throws SQLException;

    boolean isEmailExists(@Nullable final String email) throws SQLException;

    @Nullable
    User updateUserById(@Nullable final String userId, @Nullable final String email, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName) throws SQLException;

    @Nullable
    User updateUserByLogin(@Nullable final String userId, @Nullable final String login, @Nullable final String email, @Nullable final String lastName, @Nullable final String firstName, @Nullable final String middleName) throws SQLException;

}
