package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error. Status is empty.");
    }
}
