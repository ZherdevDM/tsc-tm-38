package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.LogService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private ILogService logService;

    public UserRepository(@NotNull Connection connection) {
        super(connection);
        logService = new LogService();
    }

    @Override
    protected @NotNull User fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setRole(Role.valueOf(row.getString("user_role")));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @Override
    protected @NotNull String getTableName() {
        return "USER";
    }

    @Override
    @Nullable
    public User add(@NotNull String login, @NotNull String passwordHash) throws SQLException {
        @NotNull final User user = new User(login, passwordHash);
        return add(user);
    }

    @Override
    public void addAll(@NotNull final List<User> userList) throws SQLException {
        userList.forEach(user -> {
            try {
                add(user);
            } catch (SQLException e) {
                logService.error(e);
            }
        });
    }

    @Override
    public @Nullable User add(@NotNull User user) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName()
                + " (id, login, password_hash, email, user_role, first_name, last_name, middle_name, locked) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getRole().toString());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getMiddleName());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return findById(user.getId());
    }

    @Override
    @Nullable
    public User findUserByLogin(@NotNull final String login) throws SQLException {
        @Nullable final User user;
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new EntityNotFoundException();
            user = fetch(resultSet);
        }//
        return user;
    }
    @Override
    @Nullable
    public User findUserByEmail(@NotNull final String email) throws SQLException {
        @Nullable final User user;
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE /email = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new EntityNotFoundException();
            user = fetch(resultSet);
        }
        return user;
    }

    @Override
    @Nullable
    public void removeUserById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + "  id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.execute();
        }
    }

    @Override
    @Nullable
    public void removeUserByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + "  login = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            statement.execute();
        }
    }

    @Override
    @Nullable
    public User update(@NotNull final String id, @NotNull final String login, @NotNull final String passwordHash,
                       @NotNull final String email, @NotNull final Role role, @NotNull final String lastName,
                       @NotNull final String firstName, @NotNull final String middleName) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName()
                + " SET last_name = ?, first_name = ?, middle_name = ?, email = ? WHERE id = ?,  password_hash = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, lastName);
            statement.setString(2, firstName);
            statement.setString(3, middleName);
            statement.setString(4, email);
            statement.setString(5, id);
            statement.setString(6, passwordHash);
            statement.executeUpdate();
        }
        return findById(id);
    }

    @Override
    @Nullable
    public User lockUserByLogin(@NotNull final String login) throws SQLException {
        changeLockByLogin(login, true);
        return findUserByLogin(login);
    }

    @Override
    @Nullable
    public User unLockUserByLogin(@NotNull final String login) throws SQLException {
        changeLockByLogin(login, false);
        return findUserByLogin(login);
    }

    private void changeLockByLogin(@NotNull final String login, final boolean locked) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET locked = ? WHERE login = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setBoolean(1, locked);
            statement.setString(2, login);
            statement.executeUpdate();
        }
    }

}
