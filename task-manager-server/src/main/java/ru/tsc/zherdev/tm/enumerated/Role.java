package ru.tsc.zherdev.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
