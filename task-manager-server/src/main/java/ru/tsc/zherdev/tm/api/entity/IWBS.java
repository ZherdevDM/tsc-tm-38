package ru.tsc.zherdev.tm.api.entity;

public interface IWBS extends IHasCreated, IHasName, IHasStatus, IHasStartDate {
}
