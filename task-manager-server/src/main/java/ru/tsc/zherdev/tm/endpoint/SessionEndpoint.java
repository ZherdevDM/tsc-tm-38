package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.api.service.ISessionService;
import ru.tsc.zherdev.tm.dto.Fail;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.dto.Success;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    ISessionService sessionService;

    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "partName") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) throws SQLException {
        return serviceLocator.getSessionService().login(login, password);
    }

    @Override
    @WebMethod
    @NotNull
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @Nullable
    public boolean isValidSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
        } catch (AccessDeniedException e) {
            return false;
        }
        return true;
    }

}
