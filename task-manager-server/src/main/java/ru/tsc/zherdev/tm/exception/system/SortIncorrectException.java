package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class SortIncorrectException extends AbstractException {

    public SortIncorrectException() {
        super("Error. Wrong sort type.");
    }

    public SortIncorrectException(String sortType) {
        super("Error. '" + sortType + "' is wrong sort type.");
    }

}
