package ru.tsc.zherdev.tm.dto;

import lombok.NoArgsConstructor;

public class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }
}
