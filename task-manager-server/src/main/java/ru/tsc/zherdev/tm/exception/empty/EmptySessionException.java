package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptySessionException extends AbstractException {

    public EmptySessionException() {
        super("Error. No session was sent");
    }

}
