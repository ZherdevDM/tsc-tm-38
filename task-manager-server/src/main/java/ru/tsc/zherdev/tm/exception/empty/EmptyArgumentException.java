package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyArgumentException extends AbstractException {

    public EmptyArgumentException() {
        super("Error. Argument is empty.");
    }

}
