package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity> {

    @NotNull
    E create(@NotNull final String userId, @NotNull final String name, @NotNull final String description);

    @NotNull
    E add(@NotNull final String userId, @NotNull final E entity) throws SQLException;

    void addAll(@NotNull final List<E> entityList) throws SQLException;
    
    void remove(@Nullable final String userId, @Nullable final E entity) throws SQLException;
    
    void removeByName(@Nullable final String userId, @Nullable final String name) throws SQLException;
    
    void removeById(@Nullable final String userId, @Nullable final String id) throws SQLException;
    
    void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    void clear(@Nullable final String userId) throws SQLException;

    @NotNull
    Integer getSize(@Nullable final String userId) throws SQLException;

    @Nullable
    E findByName(@Nullable final String userId, @NotNull final String name) throws SQLException;

    @Nullable
    E findById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    @Nullable
    E findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    boolean existsByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    E startByName(@Nullable final String userId, @NotNull final String name) throws SQLException;

    @Nullable
    E startById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    @Nullable
    E startByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    @Nullable
    E finishByName(@Nullable final String userId, @NotNull final String name) throws SQLException;

    @Nullable
    E finishById(@Nullable final String userId, @Nullable final String id) throws SQLException;

    @Nullable
    E finishByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException;

    @Nullable
    E changeStatusByName(@Nullable final String userId, @NotNull final String name, @NotNull final Status status) throws SQLException;

    @Nullable
    E changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) throws SQLException;

    @Nullable
    E changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @NotNull final Status status) throws SQLException;

    @NotNull
    List<E> findAll(@Nullable final String userId) throws SQLException;

    @NotNull
    List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) throws SQLException;

    @Nullable
    E updateById(@Nullable final String userId, @Nullable final String id, @NotNull final String name, @NotNull final String description) throws SQLException;

    @Nullable
    E updateByIndex(@Nullable final String userId, @Nullable final Integer index, @NotNull final String name, @NotNull final String description) throws SQLException;

}
