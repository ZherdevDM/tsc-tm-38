package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password) throws SQLException;

    @Nullable
    Session login(@Nullable final String login, @Nullable final String password) throws SQLException;

    @Nullable
    Session sign(@Nullable final Session session);

    @Nullable
    User getUser(@Nullable final Session session) throws AccessDeniedException, SQLException;

    @Nullable
    String getUserId(@Nullable final Session session) throws AccessDeniedException;

    @Nullable
    List<Session> getListSession(@Nullable final Session session) throws AccessDeniedException, SQLException;

    @Nullable
    Session open(@Nullable final Session session) throws AccessDeniedException, SQLException;

    void close(@Nullable final Session session) throws AccessDeniedException, SQLException;

    void closeAll(@Nullable final Session session) throws AccessDeniedException, SQLException;

    void validate(@Nullable final Session session) throws AccessDeniedException;

    void validateAdmin(@Nullable final Session session) throws AccessDeniedException, SQLException;

    boolean isValid(@Nullable final Session session) throws SQLException;

    void signOutByLogin(@Nullable final String login) throws SQLException;

    void signOutByUserId(@Nullable final String login) throws SQLException;

}
