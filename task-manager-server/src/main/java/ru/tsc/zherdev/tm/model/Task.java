package ru.tsc.zherdev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractOwnerBusinessEntity {

    @Nullable
    private String projectId = null;

    public Task(final String userId, @NotNull final String name, @NotNull final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return id + " : " + name;
    }

}
