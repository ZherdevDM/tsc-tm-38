package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;

public interface IAuthService {

    @Nullable
    String getUserId();

    void setUserId(final String currentUserId);

    boolean isAuth();

    @NotNull
    User login(final String login, final String password);

    @Nullable
    User userValidate(String login, String password);

    void checkRoles(final Role... roles);

    void logOut();

}
