package ru.tsc.zherdev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@RequiredArgsConstructor
public class DataConnectionService implements IDataConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ILogService logService;

    @Override
    public @NotNull Connection getConnection() throws SQLException {
        @NotNull final String url = propertyService.getJDBCurl();
        @NotNull final Properties properties = new Properties();
        properties.setProperty("user", propertyService.getJDBCUser());
        properties.setProperty("password", propertyService.getJDBCPassword());
        @NotNull final Connection connection = DriverManager.getConnection(url, properties);
        connection.setAutoCommit(false);
        return connection;
    }

    @Nullable
    private List<String> createQueriesFromFile(@Nullable final String path) {
        if (path == null && path.isEmpty()) return Collections.emptyList();
        String queryLine = "";
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<String> listOfQueries = new ArrayList<>();

        try (
                final FileReader fileReader = new FileReader(path);
                final BufferedReader bufferedReader = new BufferedReader(fileReader)
        ) {
            while ((queryLine = bufferedReader.readLine()) != null) {
                skipComments(stringBuilder, bufferedReader, queryLine);
                stringBuilder.append(queryLine).append(" ");
            }

            final String[] splitQueries = stringBuilder.toString().split(";");

            for (@NotNull final String splitQuery : splitQueries) {
                if (!splitQuery.trim().equals("") && !splitQuery.trim().equals("\t")) {
                    listOfQueries.add(splitQuery);
                }
            }
        } catch (Exception e) {
            logService.error(e);
            logService.debug(e.toString());
            logService.debug(stringBuilder.toString());
        }

        return listOfQueries;
    }

    private void skipComments(
            @NotNull final StringBuilder stringBuilder,
            @NotNull final BufferedReader br,
            @NotNull String queryLine
    ) throws IOException {
        final String[] commentSymbols = new String[]{"#", "--", "/*"};

        for (@NotNull final String ignored : commentSymbols) {
            int indexOfCommentSymbol = queryLine.indexOf(ignored);
            if (indexOfCommentSymbol != -1 && queryLine.startsWith(ignored)) queryLine = "";
            queryLine = queryLine.substring(0, indexOfCommentSymbol - 1);

            if (ignored.equals("/*")) {
                stringBuilder.append(queryLine).append(" ");
                do {
                    queryLine = br.readLine();
                }
                while (queryLine != null && !queryLine.contains("*/"));
                assert queryLine != null;
                indexOfCommentSymbol = queryLine.indexOf("*/");
                if (indexOfCommentSymbol != -1 && queryLine.endsWith("*/")) queryLine = "";
                else
                    queryLine = queryLine.substring(indexOfCommentSymbol + 2, queryLine.length() - 1);
            }
        }
    }

}
