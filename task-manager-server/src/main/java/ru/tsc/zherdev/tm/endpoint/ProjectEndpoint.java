package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.dto.Fail;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.dto.Success;
import ru.tsc.zherdev.tm.enumerated.Sort;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.entity.EmptyComparatorException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public @NotNull Project addProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @NotNull final Project project
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (session.getUserId() == null) throw new UserNotFoundException();
        return serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @WebMethod
    @Override
    public Result addAllProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects")
            @NotNull final List<Project> projectList
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().addAll(projectList);
            return new Success();
        } catch (Exception e)  {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public @NotNull Result clearProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (session.getUserId() == null) throw new UserNotFoundException();
        try {
            serviceLocator.getProjectService().clear(session.getUserId());
            return new Success();
        } catch (Exception e)  {
            return new Fail(e);
        }
    }

    @Nullable
    @WebMethod
    @Override
    public @NotNull Result removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @NotNull final Project project
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().remove(session.getUserId(), project);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public @NotNull Result removeByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeById(session.getUserId(), id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @WebMethod
    @Override
    public Result removeByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeByName(session.getUserId(), name);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }


    @WebMethod
    @Override
    public @NotNull Result removeByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public @NotNull Result clearUserProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().clear(session.getUserId());
            return new Success();
        } catch (Exception e)  {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    @NotNull
    public Integer getSizeProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().getSize(session.getUserId());
    }

    @Nullable
    @WebMethod
    @Override
    public @NotNull List<Project> findAllProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (session.getUserId() == null) throw new UserNotFoundException();
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    @Override
    public Project findByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Project findByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public boolean existsByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public boolean existsByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().existsById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public @NotNull List<Project> findAllUserProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Nullable
    @WebMethod
    @Override
    public @NotNull List<Project> findAllUserProjectSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (sort == null || sort.isEmpty()) throw new EmptyComparatorException();
        Sort sortType = null;
        try {
            sortType = Sort.valueOf(sort);
        } catch (IllegalArgumentException e) {
            throw new EmptyComparatorException();
        }
        @NotNull final Comparator<Project> comparator = (Comparator<Project>) sortType.getComparator();
        return serviceLocator.getProjectService().findAll(session.getUserId(), comparator);
    }

    @Nullable
    @WebMethod
    @Override
    public Project findByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Project createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Nullable
    @WebMethod
    @Override
    public Project updateByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
    }

    @Nullable
    @WebMethod
    @Override
    public Project updateByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @WebMethod
    @Override
    public Project startByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Project startByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Project startByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @Override
    public Project finishByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Project finishByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Project finishByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @Override
    public Project changeStatusByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @Nullable
    @WebMethod
    @Override
    public Project changeStatusByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status);
    }

    @Nullable
    @WebMethod
    @Override
    public Project changeStatusByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

}
