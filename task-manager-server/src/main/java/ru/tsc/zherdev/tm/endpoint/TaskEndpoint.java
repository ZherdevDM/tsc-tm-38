package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.dto.Fail;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.dto.Success;
import ru.tsc.zherdev.tm.enumerated.Sort;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @WebMethod
    @Override
    public Task createTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @NotNull
    @WebMethod
    @Override
    public Task addTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @NotNull final Task entity
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), entity);
    }

    @WebMethod
    @Override
    public Result addAllTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "tasks") @NotNull final List<Task> tasks
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().addAll(tasks);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public @NotNull List<Task> findAllTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @NotNull
    @WebMethod
    @Override
    public Result removeUserTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @NotNull final Task task
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().remove(session.getUserId(), task);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @WebMethod
    @Override
    public Result removeByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
        try {
            serviceLocator.getTaskService().removeByName(session.getUserId(), name);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @WebMethod
    @Override
    public Result removeByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().removeById(session.getUserId(), id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @WebMethod
    @Override
    public Result removeByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().clear(session.getUserId());
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearUserTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getTaskService().clear(session.getUserId());
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @WebMethod
    @Override
    public Integer getSizeTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getSize(session.getUserId());
    }

    @Nullable
    @WebMethod
    @Override
    public Task findByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Task findByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Task findByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public boolean existsByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public boolean existsByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().existsById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Task startByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Task startByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Task startByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @Override
    public Task finishByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @Override
    public Task finishByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @Nullable
    @WebMethod
    @Override
    public Task finishByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @Override
    public Task changeStatusByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @Nullable
    @WebMethod
    @Override
    public Task changeStatusByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @Nullable
    @WebMethod
    @Override
    public Task changeStatusByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    @Override
    public @NotNull List<Task> findAllUserTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    public @NotNull List<Task> findAllUserTaskSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable String sort
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (!Sort.isValid(sort)) throw new SortIncorrectException();
        @NotNull final Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Task> comparator = (Comparator<Task>) sortType.getComparator();
        return serviceLocator.getTaskService().findAll(session.getUserId(), comparator);
    }

    @Nullable
    @WebMethod
    @Override
    public Task updateByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @Nullable
    @WebMethod
    @Override
    public Task updateByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

}
