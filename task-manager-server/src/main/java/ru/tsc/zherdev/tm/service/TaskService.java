package ru.tsc.zherdev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.exception.empty.EmptyLoginException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.empty.EmptyTaskIdException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractOwnerBusinessService<Task> implements IOwnerBusinessService<Task> {

    public TaskService(@NotNull final IDataConnectionService connectionService,
                       @NotNull final ILogService logService) {
        super(connectionService, logService);
    }

    @Override
    protected @NotNull ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    @SneakyThrows
    public @NotNull Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable String description
    ) {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        if (description == null)
            description = "";
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository repository = getRepository(connection);
            @Nullable final Task task = repository.create(userId, name, description);
            connection.commit();
            return task;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void addAll(@NotNull List<Task> taskList) throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository repository = getRepository(connection);
            repository.addAll(taskList);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public @NotNull Task add(@Nullable String userId, @Nullable Task task) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository repository = getRepository(connection);
            if (task == null) {
                logService.error(new TaskNotFoundException());
                throw new TaskNotFoundException();
            }
            task = repository.add(userId, task);
            connection.commit();
            return task;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

}
