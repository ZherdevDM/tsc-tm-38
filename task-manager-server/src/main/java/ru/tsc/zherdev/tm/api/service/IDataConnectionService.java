package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDataConnectionService {

    @NotNull
    Connection getConnection() throws SQLException;

}
