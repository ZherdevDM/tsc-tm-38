package ru.tsc.zherdev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Result {

    public Boolean success = true;

    public String message = "";

}
