package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public final class ProjectService extends AbstractOwnerBusinessService<Project> implements IOwnerBusinessService<Project> {

    public ProjectService(@NotNull final IDataConnectionService connectionService,
                          @NotNull final ILogService logService) {
        super(connectionService, logService);
    }

    @Override
    protected @NotNull IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    public @NotNull Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        if (description == null)
            description = "";
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull IProjectRepository repository = getRepository(connection);
            @Nullable final Project project = repository.create(userId, name, description);
            connection.commit();
            return project;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public @NotNull Project add(@Nullable String userId, @NotNull Project project) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull IProjectRepository repository = getRepository(connection);
            project = repository.add(userId, project);
            connection.commit();
            return project;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void addAll(@NotNull List<Project> projectList) throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull IProjectRepository repository = getRepository(connection);
            repository.addAll(projectList);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

}
