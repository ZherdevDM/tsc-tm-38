package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getSignatureIteration();

    @NotNull
    String getSignatureSecret();

}
