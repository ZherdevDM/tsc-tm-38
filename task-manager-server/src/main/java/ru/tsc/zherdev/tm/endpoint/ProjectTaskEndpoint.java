package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.dto.Fail;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.dto.Success;
import ru.tsc.zherdev.tm.enumerated.Sort;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @Nullable List<Task> findTaskByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public @Nullable List<Task> findTaskByProjectIdSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "sort") @Nullable final Sort sort
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        if (sort == null) {
            @NotNull final Comparator<Task> comparator = (Comparator<Task>) Sort.NAME.getComparator();
            return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId, comparator);
        }
        if (!Sort.isValid(sort.getDisplayName())) throw new SortIncorrectException();
        @NotNull final Comparator<Task> comparator = (Comparator<Task>) sort.getComparator();
        return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId, comparator);
    }

    @Override
    @WebMethod
    public @Nullable Task bindTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public @Nullable Task unbindTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public @Nullable Result removeProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

}
