package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.dto.Domain;

public interface IDomainRepository {
    @NotNull Domain getDomain();

    void setDomain(@NotNull Domain domain);

    void setUsers(@NotNull Domain domain);

    void setData(@NotNull Domain domain);
}
