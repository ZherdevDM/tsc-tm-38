package ru.tsc.zherdev.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IProjectTaskService;
import ru.tsc.zherdev.tm.exception.empty.EmptyProjectIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyTaskIdException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.repository.ProjectRepository;
import ru.tsc.zherdev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IDataConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    @NotNull
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull IProjectRepository  projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            return taskRepository.findTaskByProjectId(userId, projectId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable String projectId,
                                          @Nullable Comparator<Task> comparator) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull IProjectRepository  projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (comparator == null) return taskRepository.findTaskByProjectId(userId, projectId);
            return taskRepository.findTaskByProjectId(userId, projectId, comparator);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public Task bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        Optional.ofNullable(taskId).orElseThrow(EmptyTaskIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull IProjectRepository  projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
            @Nullable final Task task = taskRepository.bindTaskById(userId, projectId, taskId);
            connection.commit();
            return task;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public Task unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        Optional.ofNullable(taskId).orElseThrow(EmptyTaskIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull IProjectRepository  projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
            @Nullable final Task task = taskRepository.unbindTaskById(userId, projectId, taskId);
            connection.commit();
            return task;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(projectId).orElseThrow(EmptyProjectIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull IProjectRepository  projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            findTaskByProjectId(userId, projectId).forEach(o -> {
                try {
                    taskRepository.removeById(userId, o.getId());
                } catch (SQLException e) {
                    logService.error(e);
                }
            });
            projectRepository.removeById(userId, projectId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

}
