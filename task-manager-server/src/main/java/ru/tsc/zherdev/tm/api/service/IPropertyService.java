package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerProtocol();

    @NotNull
    String getJDBCurl();

    @NotNull
    String getJDBCUser();

    @NotNull
    String getJDBCPassword();

}
