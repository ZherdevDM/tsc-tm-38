package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class SaltServiceIncorrectException extends AbstractException {

    public SaltServiceIncorrectException() {
        super("Error. Check project's application.properties.");
    }
}
