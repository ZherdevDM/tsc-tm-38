package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ISessionRepository;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.service.LogService;

import java.sql.*;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final ILogService logService;

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
        logService = new LogService();
    }

    @Override
    protected @NotNull Session fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setTimestamp(row.getDate("login_timestamp").getTime());
        session.setSignature(row.getString("signature"));
        session.setUserId(row.getString("user_id"));
        return session;
    }

    @Override
    protected @NotNull String getTableName() {
        return "SESSION";
    }

    @Override
    @Nullable
    public Session open(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, date, signature, user_id)" +
                " VALUES (?, ?, ?, ?);";
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, session.getTimestamp() == null ? null : new Timestamp(session.getTimestamp()));
            statement.setString(3, session.getSignature());
            statement.setString(4, session.getUserId());
            statement.executeUpdate();
        }
        return session;
    }

    @Override
    public void addAll(@NotNull List<Session> sessionList) throws SQLException {
        sessionList.forEach(session -> {
            try {
                open(session);
            } catch (SQLException e) {
                logService.error(e);
            }
        });
    }

    @Override
    public boolean close(@NotNull final Session session) throws SQLException {
        final int count;
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, session.getId());
            count = statement.executeUpdate();
        }
        return count > 0;
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) throws SQLException {
        final int count;
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?;";
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            count = statement.executeUpdate();
        }
        return count > 0;
    }

    @Override
    public boolean contains(@NotNull final String id) throws SQLException {
        final int count;
        @NotNull final String query = "SELECT COUNT(1) FROM " + getTableName() + " WHERE id = ?;";
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            count = statement.executeQuery().getInt(1);
        }
        return count == 1;
    }

}
