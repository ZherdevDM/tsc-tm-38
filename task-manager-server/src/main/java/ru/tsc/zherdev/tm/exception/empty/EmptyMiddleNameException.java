package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error. Middle Name is empty.");
    }

}
