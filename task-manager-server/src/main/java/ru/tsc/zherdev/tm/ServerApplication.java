package ru.tsc.zherdev.tm;

import ru.tsc.zherdev.tm.component.Bootstrap;

import java.sql.SQLException;

public class ServerApplication {

    public static void main(final String[] args) throws SQLException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
