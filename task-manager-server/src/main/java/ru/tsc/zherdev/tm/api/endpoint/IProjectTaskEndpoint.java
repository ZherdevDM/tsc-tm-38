package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.enumerated.Sort;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;
import java.util.List;

public interface IProjectTaskEndpoint extends PublishedEndpoint {

    @WebMethod
    @Nullable List<Task> findTaskByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws SQLException;

    @WebMethod
    @Nullable List<Task> findTaskByProjectIdSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "sort") @Nullable final Sort sort
    ) throws SQLException;

    @WebMethod
    @Nullable Task bindTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws SQLException;

    @WebMethod
    @Nullable Task unbindTaskById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws SQLException;

    @WebMethod
    @Nullable Result removeProjectById(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws SQLException;

}
