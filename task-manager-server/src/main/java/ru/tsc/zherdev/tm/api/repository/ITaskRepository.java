package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IOwnerBusinessRepository<Task> {

    @NotNull
    List<Task> findTaskByProjectId(final String userId, final String projectId) throws SQLException;

    @Nullable
    List<Task> findTaskByProjectId(final String userId, final String projectId, final Comparator<Task> comparator) throws SQLException;

    @Nullable
    Task bindTaskById(final String userId, final String projectId, final String taskId) throws SQLException;

    @Nullable
    Task unbindTaskById(final String userId, final String projectId, final String taskId) throws SQLException;

}
