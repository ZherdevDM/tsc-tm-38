package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @Nullable
    public User getUserSelfByUserId(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findUserById(session.getUserId());
    }

    @Override
    @WebMethod
    @NotNull
    public User setPasswordUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUserByUserId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserById(
                session.getUserId(), lastName, firstName, middleName, email
        );
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUserByUserLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserByLogin(
                session.getUserId(), login, lastName, firstName, middleName, email
        );
    }

}
