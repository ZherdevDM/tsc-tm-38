package ru.tsc.zherdev.tm.exception.system;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class SQLExecutionFailedException extends AbstractException {

    public SQLExecutionFailedException() {
        super("Error. Query failed to execute.");
    }

}
