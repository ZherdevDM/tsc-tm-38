package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void clear() throws SQLException;

    void remove(final E entity) throws SQLException;

    void removeById(final String id) throws SQLException;

    void removeByIndex(final Integer index) throws SQLException;

    @NotNull
    List<E> findAll() throws SQLException;

    @NotNull
    List<E> findAll(final Comparator<E> comparator) throws SQLException;

    @Nullable
    E findById(final String id) throws SQLException;

    @Nullable
    E findByIndex(final Integer index) throws SQLException;

    boolean existsById(final String id) throws SQLException;

    boolean existsByIndex(final Integer index) throws SQLException;

    Integer getSize() throws SQLException;

}
