package ru.tsc.zherdev.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        int numberValue;
        try {
            numberValue = Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
        return numberValue;
    }

}