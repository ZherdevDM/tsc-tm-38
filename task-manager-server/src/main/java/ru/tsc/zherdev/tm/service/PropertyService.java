package ru.tsc.zherdev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_NAME_KEY = "developer.name";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PROTOCOL_DEFAULT = "";

    @NotNull
    private static final String SERVER_PROTOCOL_KEY = "server.protocol";

    @NotNull
    private static final String SIGNATURE_SECRET_DEFAULT = "";

    @NotNull
    private static final String SIGNATURE_SECRET_KEY = "signature.secret";

    @NotNull
    private static final String SIGNATURE_ITERATION_DEFAULT = "";

    @NotNull
    private static final String SIGNATURE_ITERATION_KEY = "signature.iteration";

    @NotNull
    private static final String JDBC_URL_DEFAULT = "";

    @NotNull
    private static final String JDBC_URL_KEY = "db.jdbc.url";

    @NotNull
    private static final String JDBC_USER_DEFAULT = "";

    @NotNull
    private static final String JDBC_USER_KEY = "db.jdbc.user";

    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT = "";

    @NotNull
    private static final String JDBC_PASSWORD_KEY = "db.jdbc.password";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getProperty(final String keyValue, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(keyValue);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(keyValue);
        if (environmentProperty != null) return environmentProperty;

        return properties.getProperty(keyValue, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public @NotNull Integer getPasswordIteration() {
        return Integer.parseInt(getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @Override
    public @NotNull String getPasswordSecret() {
        return getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperName() {
        return getProperty(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @Override
    public @NotNull String getDeveloperEmail() {
        return getProperty(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    public @NotNull String getSignatureSecret() {
        return getProperty(SIGNATURE_SECRET_KEY, SIGNATURE_SECRET_DEFAULT);
    }

    @Override
    public @NotNull Integer getSignatureIteration() {
        return Integer.parseInt(getProperty(SIGNATURE_ITERATION_KEY, SIGNATURE_ITERATION_DEFAULT));
    }

    @Override
    public @NotNull String getServerHost() {
        return getProperty(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getServerProtocol() {
        return getProperty(SERVER_PROTOCOL_KEY, SERVER_PROTOCOL_DEFAULT);
    }

    @Override
    public @NotNull String getJDBCurl() { return getProperty(JDBC_URL_KEY, JDBC_URL_DEFAULT); }

    @Override
    public @NotNull String getJDBCUser() { return getProperty(JDBC_USER_KEY, JDBC_USER_DEFAULT); }

    @Override
    public @NotNull String getJDBCPassword() { return getProperty(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT); }

}
