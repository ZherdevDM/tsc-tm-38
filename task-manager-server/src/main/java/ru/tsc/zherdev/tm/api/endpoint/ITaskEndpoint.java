package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskEndpoint extends PublishedEndpoint {
    @Nullable
    @WebMethod
    Task createTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    ) throws SQLException;

    @NotNull
    @WebMethod
    Task addTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @NotNull final Task entity
    ) throws SQLException;

    @WebMethod
    Result addAllTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "tasks") @NotNull final List<Task> tasks
    ) throws SQLException;

    @WebMethod
    @NotNull List<Task> findAllTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @NotNull
    @WebMethod
    Result removeUserTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @NotNull Task task
    ) throws SQLException;

    @Nullable
    @WebMethod
    Result removeByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Result removeByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Result removeByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index
    ) throws SQLException;

    @WebMethod
    Result clearTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    Result clearUserTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @NotNull
    @WebMethod
    Integer getSizeTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task findByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task findByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task findByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index
    ) throws SQLException;

    @WebMethod
    boolean existsByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index
    ) throws SQLException;

    @WebMethod
    boolean existsByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task startByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task startByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task startByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task finishByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task finishByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task finishByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task changeStatusByNameTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "status") @NotNull Status status
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task changeStatusByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "status") @NotNull Status status
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task changeStatusByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "status") @NotNull Status status
    ) throws SQLException;

    @WebMethod
    @NotNull List<Task> findAllUserTask(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    @NotNull List<Task> findAllUserTaskSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable String sort
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task updateByIdTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull String id,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    ) throws SQLException;

    @Nullable
    @WebMethod
    Task updateByIndexTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull Integer index,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull String description
    ) throws SQLException;
}
