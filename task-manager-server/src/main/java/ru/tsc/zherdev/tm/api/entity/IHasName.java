package ru.tsc.zherdev.tm.api.entity;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    String getName();

    void setName(@NotNull final String name);

}
