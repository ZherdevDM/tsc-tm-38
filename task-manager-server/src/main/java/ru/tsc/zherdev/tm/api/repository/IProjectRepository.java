package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.model.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IOwnerBusinessRepository<Project> {

}
