package ru.tsc.zherdev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.zherdev.tm.api.service.IServiceLocator;
import ru.tsc.zherdev.tm.dto.Fail;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.dto.Success;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.enumerated.Sort;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SortIncorrectException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    @NotNull
    public User addUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        if (user == null) throw new UserNotFoundException();
        return serviceLocator.getUserService().add(user);
    }

    @Override
    @WebMethod
    public Result addAllUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "collection") @Nullable final List<User> userList
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        if (userList == null) throw new UserNotFoundException();
        try {
            serviceLocator.getUserService().addAll(userList);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public Result clearUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().clear();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().remove(user);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeById(id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeByIndex(index);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeUserById(id);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    public @NotNull Result removeUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        try {
            serviceLocator.getUserService().removeUserByLogin(login);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @NotNull
    public List<User> findAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    @NotNull
    public List<User> findAllUserSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        Sort sortType = null;
        try {
            sortType = Sort.valueOf(sort);
        } catch (IllegalArgumentException e) {
            throw new SortIncorrectException();
        }
        @NotNull final Comparator<User> comparator = (Comparator<User>) sortType.getComparator();
        return serviceLocator.getUserService().findAll(comparator);
    }

    @Override
    @WebMethod
    @Nullable
    public User findByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @WebMethod
    @Nullable
    public User findByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findByIndex(index);
    }

    @Override
    @WebMethod
    public boolean existsByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().existsById(id);
    }

    @Override
    @WebMethod
    public boolean existsByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().existsByIndex(index);
    }

    @Override
    @WebMethod
    @Nullable
    public Integer getSizeUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().getSize();
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByEmailUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findUserByEmail(email);
    }

    @Override
    @WebMethod
    @Nullable
    public User lockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    @Nullable
    public User unLockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().unLockUserByLogin(login);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().add(login, password);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUserExtraEmail(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    @NotNull
    public User createUserExtraRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    @Nullable
    public User updateUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "passwordHash") @Nullable final String passwordHash,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "role") @Nullable final Role role,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().update(
                userId, login, passwordHash, email, role, lastName, firstName, middleName
        );
    }

    @Override
    @WebMethod
    @Nullable
    public User setRoleUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().setRole(session.getUserId(), role);
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException {
        serviceLocator.getSessionService().validateAdmin(session);
        return serviceLocator.getUserService().findUserById(id);
    }

    @Override
    @WebMethod
    public boolean isLoginExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isLoginExists(login);
    }

    @Override
    @WebMethod
    public boolean isEmailExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isEmailExists(email);
    }

}
