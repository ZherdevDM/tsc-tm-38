package ru.tsc.zherdev.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class Project extends AbstractOwnerBusinessEntity {

    public Project(@NotNull final String userId, @NotNull final String name) {
        this.userId = userId;
        this.name = name;
    }

    public Project(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        return id + " | name: " + name + " | description: " + description;
    }

}
