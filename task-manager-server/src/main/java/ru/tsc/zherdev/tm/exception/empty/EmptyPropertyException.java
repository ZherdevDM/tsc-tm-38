package ru.tsc.zherdev.tm.exception.empty;

import ru.tsc.zherdev.tm.exception.AbstractException;

public class EmptyPropertyException extends AbstractException {

    public EmptyPropertyException() {
        super("Error. Required property value missing.");
    }

}
