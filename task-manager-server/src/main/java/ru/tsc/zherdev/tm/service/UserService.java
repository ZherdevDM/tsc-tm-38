package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IUserRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.api.service.IUserService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.empty.*;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.UserNotFoundException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.exception.user.EmptyHashException;
import ru.tsc.zherdev.tm.exception.user.EmptyUserException;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.UserRepository;
import ru.tsc.zherdev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IDataConnectionService connectionService,
                       @NotNull final IPropertyService propertyService,
                       @NotNull final ILogService logService) {
        super(connectionService, logService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    public String getPasswordHash(@Nullable final String password) {
        return HashUtil.salt(propertyService, password);
    }

    @Override
    @NotNull
    public User add(@Nullable final String login, @Nullable final String password) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = getPasswordHash(password);
        @Nullable final User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = userRepository.add(login, passwordHash);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        if (user == null) {
            logService.error(new UserNotFoundException());
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public @Nullable User add(@NotNull User user) throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    public void addAll(@NotNull List<User> userList) throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            userRepository.addAll(userList);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            userRepository.add(login, password);
            connection.commit();
            user = findUserByLogin(login);
            user = updateUserByLogin(user.getId(), login, null, null, null, email);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        if (user == null) {
            logService.error(new UserNotFoundException());
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            userRepository.add(login, password);
            connection.commit();
            user = findUserByLogin(login);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            user.setRole(role);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String userId, @Nullable final String password) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = findUserById(userId);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            String passwordHash = getPasswordHash(password);
            user = userRepository.update(
                    user.getId(), user.getLogin(), passwordHash, user.getEmail(), user.getRole(),
                    user.getLastName(), user.getFirstName(), user.getMiddleName()
            );
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    @Nullable
    public User setRole(@Nullable final String userId, @Nullable final Role role) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = findUserById(userId);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            user = userRepository.update(
                    user.getId(), user.getLogin(), user.getPasswordHash(), user.getEmail(), role,
                    user.getLastName(), user.getFirstName(), user.getMiddleName()
            );
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    @Nullable
    public User findUserById(@Nullable final String userId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = userRepository.findById(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new UserNotFoundException();
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = userRepository.findUserByLogin(login);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    @Nullable
    public User findUserByEmail(@Nullable final String email) throws SQLException {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = userRepository.findUserByEmail(email);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return user;
    }

    @Override
    public void removeUserById(@Nullable final String userId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            removeById(userId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            removeUserByLogin(login);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public User userValidate(@Nullable String login, @Nullable String password) throws SQLException {
        @Nullable User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            user = findUserByLogin(login);
            if (user == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final String hash = getPasswordHash(password);
        Optional.ofNullable(hash).orElseThrow(EmptyHashException::new);
        if (!hash.equals(user.getPasswordHash())) return null;
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            return findUserByLogin(login) != null;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws SQLException {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            return findUserByEmail(email) != null;
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public User update(@Nullable String id, @Nullable String login, @Nullable String password,
                       @Nullable String email, @Nullable Role role, @Nullable String lastName,
                       @Nullable String firstName, @Nullable String middleName) throws SQLException {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        Optional.ofNullable(lastName).orElseThrow(EmptyLastNameException::new);
        Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.ofNullable(middleName).orElseThrow(EmptyMiddleNameException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        @Nullable User updatedUser;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            updatedUser = userRepository.findById(id);
            if (updatedUser == null) {
                logService.error(new UserNotFoundException());
                throw new UserNotFoundException();
            }
            final String passwordHash = getPasswordHash(password);
            Optional.ofNullable(passwordHash).orElseThrow(EmptyHashException::new);
            if (!updatedUser.getLogin().equals(login)) updatedUser.setLogin(login);
            if (!updatedUser.getPasswordHash().equals(passwordHash)) updatedUser.setPasswordHash(passwordHash);
            if (!updatedUser.getEmail().equals(email)) updatedUser.setEmail(email);
            if (updatedUser.getRole() != role) updatedUser.setRole(role);
            if (!updatedUser.getLastName().equals(lastName)) updatedUser.setLastName(lastName);
            if (!updatedUser.getFirstName().equals(firstName)) updatedUser.setFirstName(firstName);
            if (!updatedUser.getMiddleName().equals(middleName)) updatedUser.setMiddleName(middleName);
            updatedUser = userRepository.update(updatedUser.getId(), updatedUser.getLogin(),
                    updatedUser.getPasswordHash(), updatedUser.getEmail(), updatedUser.getRole(),
                    updatedUser.getLastName(), updatedUser.getFirstName(), updatedUser.getMiddleName());
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return updatedUser;
    }

    @Override
    @Nullable
    public User updateUserById(@Nullable final String userId, @Nullable final String lastName,
                               @Nullable final String firstName, @Nullable final String middleName,
                               @Nullable final String email) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        @Nullable final User user = findUserById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        return update(userId, user.getLogin(), user.getPasswordHash(), email, user.getRole(), lastName, firstName, middleName);
    }

    @Override
    @Nullable
    public User updateUserByLogin(@Nullable final String userId, @Nullable final String login,
                                  @Nullable final String lastName, @Nullable final String firstName,
                                  @Nullable final String middleName, @Nullable final String email) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(EmptyUserException::new);
        return update(user.getId(), login, user.getPasswordHash(), email, user.getRole(), lastName, firstName, middleName);
    }

    @Override
    @Nullable
    public User lockUserByLogin(@Nullable final String login) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            return userRepository.lockUserByLogin(login);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public User unLockUserByLogin(@Nullable final String login) throws SQLException {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        @Nullable final User user = findUserByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IUserRepository userRepository = getRepository(connection);
            return userRepository.unLockUserByLogin(login);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

}
