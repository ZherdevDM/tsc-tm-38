package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User add(@NotNull String login, @NotNull String passwordHash) throws SQLException;

    @Nullable
    User add(@NotNull User user) throws SQLException;

    void addAll(@NotNull final List<User> userList) throws SQLException;

    @Nullable
    User findUserByLogin(@NotNull final String login) throws SQLException;

    @Nullable
    User findUserByEmail(final String email) throws SQLException;

    @Nullable
    void removeUserById(final String id) throws SQLException;

    @Nullable
    void removeUserByLogin(final String login) throws SQLException;

    @Nullable
    User update(final String id, final String login, final String passwordHash, final String email,
                final Role role, final String lastName, final String firstName, final String middleName) throws SQLException;

    @Nullable
    User lockUserByLogin(final String login) throws SQLException;

    @Nullable
    User unLockUserByLogin(final String login) throws SQLException;

}
