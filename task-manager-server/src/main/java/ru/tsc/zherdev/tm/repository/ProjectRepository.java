package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.service.LogService;

import java.sql.*;
import java.util.List;

public final class ProjectRepository extends AbstractOwnerBusinessRepository<Project> implements IProjectRepository {

    @NotNull
    private ILogService logService;

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
        logService = new LogService();
    }

    @Override
    protected @NotNull Project fetch(@NotNull final ResultSet row) throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setCreated(row.getTimestamp("create_date"));
        project.setStartDate(row.getTimestamp("start_date"));
        return project;
    }

    @Override
    protected @NotNull String getTableName() {
        return "PROJECT";
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() + " (id, name, description, status, create_date, start_date, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().toString());
            statement.setTimestamp(5, new Timestamp(project.getCreated().getTime()));
            statement.setTimestamp(6, project.getStartDate() == null ? null : new Timestamp(project.getStartDate().getTime()));
            statement.setString(7, project.getUserId());
            statement.executeUpdate();
        }
        @Nullable final Project resultProject = findById(userId, project.getId());
        if (resultProject == null || resultProject.getUserId() == null) throw new TaskNotFoundException();
        return resultProject;
    }

    @Override
    public void addAll(@NotNull final List<Project> projectList) throws SQLException {
        projectList.forEach(project -> {
            try {
                if (project.getUserId() == null) throw new EmptyUserIdException();
                add(project.getUserId(), project);
            } catch (SQLException e) {
                logService.error(e);
            }
        });
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project(userId, name, description);
        return project;
    }

}
