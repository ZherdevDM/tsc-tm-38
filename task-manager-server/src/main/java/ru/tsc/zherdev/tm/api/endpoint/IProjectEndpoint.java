package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectEndpoint extends PublishedEndpoint {

    @WebMethod
    @NotNull Project addProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @NotNull final Project project
    ) throws SQLException;

    @WebMethod
    Result addAllProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projects", partName = "projects")
            @NotNull final List<Project> projectList
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result clearProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @NotNull final Project project
    ) throws SQLException;

    @NotNull
    @WebMethod
    Result removeByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException;

    @NotNull
    @WebMethod
    Result removeByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException;

    @NotNull
    @WebMethod
    Result removeByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result clearUserProject(
            @WebParam(name = "session") @Nullable Session session
    ) throws SQLException;

    @WebMethod
    @NotNull Integer getSizeProject(
            @WebParam(name = "session") @Nullable Session session
    ) throws SQLException;

    @Nullable
    @WebMethod
    @NotNull List<Project> findAllProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project findByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project findByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException;

    @WebMethod
    boolean existsByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException;

    @WebMethod
    boolean existsByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    @NotNull List<Project> findAllUserProject(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @Nullable
    @WebMethod
    @NotNull List<Project> findAllUserProjectSorted(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project findByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project createProject(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @NotNull String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project updateByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project updateByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project startByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project startByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project startByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project finishByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project finishByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project finishByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project changeStatusByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project changeStatusByIdProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException;

    @Nullable
    @WebMethod
    Project changeStatusByIndexProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final Status status
    ) throws SQLException;

}
