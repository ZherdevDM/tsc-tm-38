package ru.tsc.zherdev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    Session open(@NotNull final Session session) throws SQLException;

    void addAll(@NotNull final List<Session> sessionList) throws SQLException;

    boolean close(@NotNull final Session session) throws SQLException;

    boolean removeByUserId(@NotNull String userID) throws SQLException;

    boolean contains(@NotNull final String id) throws SQLException;

}
