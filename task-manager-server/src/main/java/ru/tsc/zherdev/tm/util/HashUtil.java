package ru.tsc.zherdev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.service.ISaltSettings;
import ru.tsc.zherdev.tm.exception.empty.EmptyIndexException;
import ru.tsc.zherdev.tm.exception.empty.EmptyPasswordException;
import ru.tsc.zherdev.tm.exception.empty.EmptyPropertyException;
import ru.tsc.zherdev.tm.exception.empty.EmptySessionException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.system.SaltServiceIncorrectException;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@UtilityClass
public final class HashUtil {

    @NotNull
    public static String salt(@Nullable final ISaltSettings settings, @Nullable final String value) {
        if (settings == null) throw new SaltServiceIncorrectException();
        @Nullable final String secret = settings.getPasswordSecret();
        @Nullable final Integer iteration = settings.getPasswordIteration();

        return salt(value, secret, iteration);
    }

    private static String salt(@Nullable final String value,
                               @Nullable final String secret,
                               @Nullable final Integer iteration
    ) {
        Optional.ofNullable(secret).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(iteration).orElseThrow(EmptyIndexException::new);
        Optional.ofNullable(value).orElseThrow(EmptyPasswordException::new);
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    private static String md5(@NotNull final String value) {
        try {
            java.security.MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (byte b : array)
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    public static String sign(@Nullable final Object value,
                              @Nullable final String secret,
                              @Nullable final Integer iteration
    ) {
        Optional.ofNullable(value).orElseThrow(EmptySessionException::new);
        Optional.ofNullable(secret).orElseThrow(EmptyPropertyException::new);
        Optional.ofNullable(iteration).orElseThrow(EmptyPropertyException::new);
        String signature = "";
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return salt(json, secret, iteration);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

}
