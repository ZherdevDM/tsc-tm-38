package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;

public interface ISessionEndpoint extends PublishedEndpoint {
    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "partName") String login,
            @WebParam(name = "password", partName = "password") String password
    ) throws SQLException;

    @WebMethod
    Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );

    @WebMethod
    @Nullable boolean isValidSession(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    );
}
