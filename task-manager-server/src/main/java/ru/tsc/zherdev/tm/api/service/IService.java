package ru.tsc.zherdev.tm.api.service;

import ru.tsc.zherdev.tm.api.repository.IRepository;
import ru.tsc.zherdev.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
