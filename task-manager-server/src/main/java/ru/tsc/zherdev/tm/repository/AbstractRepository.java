package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IRepository;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected Connection connection;


    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract E fetch(@NotNull final ResultSet row) throws AbstractException, SQLException;

    @NotNull
    protected abstract String getTableName();

    @Override
    public void clear() throws AbstractException, SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + ";";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query);) {
            statement.execute();
        }
    }

    @Override
    public void remove(@NotNull final E entity) throws AbstractException, SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.execute();
        }
    }

    @Override
    public void removeById(@NotNull String id) throws AbstractException, SQLException {
        @Nullable final E entity = findById(id);
        if (entity == null) return;
        remove(entity);
    }

    @Override
    public void removeByIndex(@NotNull final Integer index) throws AbstractException, SQLException {
        if (!existsByIndex(index)) throw new IndexIncorrectException();
        @Nullable final E entity = findByIndex(index);
        if (entity == null) throw new EntityNotFoundException();
        remove(entity);
    }

    @Override
    @NotNull
    public List<E> findAll() throws AbstractException, SQLException {
        @NotNull final List<E> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + ";";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull Comparator<E> comparator) throws AbstractException, SQLException {
        @NotNull final List<E> entitiesList = findAll();
        return entitiesList.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    @Nullable
    public E findById(@NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final E entity;
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new EntityNotFoundException();
            entity = fetch(resultSet);
        }
        return entity;
    }

    @Override
    @Nullable
    public E findByIndex(@NotNull final Integer index) throws AbstractException, SQLException  {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " LIMIT 1 OFFSET ?;";
        @NotNull final E entity;
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) throw new IndexIncorrectException();
            entity = fetch(resultSet);
        }
        return entity;
    }

    @Override
    public boolean existsById(@NotNull final String id) throws AbstractException, SQLException {
        @Nullable final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) throws AbstractException, SQLException {
        return (index >= 0 && index < getSize());
    }

    @Override
    @NotNull
    public Integer getSize() throws AbstractException, SQLException {
        @NotNull final List<E> entitiesList = findAll();
        return entitiesList.size();
    }

}
