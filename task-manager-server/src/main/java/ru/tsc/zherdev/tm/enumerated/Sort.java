package ru.tsc.zherdev.tm.enumerated;

import ru.tsc.zherdev.tm.comparator.ComparatorByCreated;
import ru.tsc.zherdev.tm.comparator.ComparatorByName;
import ru.tsc.zherdev.tm.comparator.ComparatorByStartDate;
import ru.tsc.zherdev.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created date", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    private static final String[] SORT_TYPES = new String[]{
            "NAME", "CREATED", "START_DATE", "STATUS"
    };

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public static boolean isValid(final String sort) {
        if (sort == null || sort.isEmpty()) return false;
        for (String sortType : SORT_TYPES) {
            if (sort.equals(sortType)) return true;
        }
        return false;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
