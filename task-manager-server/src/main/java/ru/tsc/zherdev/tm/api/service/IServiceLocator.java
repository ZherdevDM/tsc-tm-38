package ru.tsc.zherdev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;

public interface IServiceLocator {

    @NotNull
    IOwnerBusinessService<Task> getTaskService();

    @NotNull
    IOwnerBusinessService<Project> getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ILogService getLogService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

}
