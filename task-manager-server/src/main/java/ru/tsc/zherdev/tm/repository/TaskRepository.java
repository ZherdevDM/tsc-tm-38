package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.AbstractException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.service.LogService;

import java.sql.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    private final ILogService logService;

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
        logService = new LogService();
    }

    @Override
    protected @NotNull Task fetch(@NotNull final ResultSet row) throws AbstractException, SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setCreated(row.getTimestamp("create_date"));
        task.setStartDate(row.getTimestamp("start_date"));
        return task;
    }

    @NotNull
    @Override
    public String getTableName() {
        return "TASK";
    }

    @Override
    public @NotNull Task add(@NotNull final String userId, @NotNull final Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName()
                + " (id, name, description, status, create_date, start_date, user_id, project_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getStatus().toString());
            statement.setTimestamp(5, new Timestamp(task.getCreated().getTime()));
            statement.setTimestamp(6, task.getStartDate() == null ? null : new Timestamp(task.getStartDate().getTime()));
            statement.setString(7, task.getUserId());
            statement.setString(8, task.getProjectId());
            statement.executeUpdate();
        }
        @Nullable final Task resultTask = findById(userId, task.getId());
        if (resultTask == null || resultTask.getUserId() == null) throw new TaskNotFoundException();
        return resultTask;
    }

    @Override
    public void addAll(@NotNull List<Task> taskList) throws SQLException {
        taskList.forEach(task -> {
            try {
                if (task.getUserId() == null) throw new EmptyUserIdException();
                add(task.getUserId(), task);
            } catch (SQLException e) {
                logService.error(e);
            }
        });
    }

    @Override
    public @NotNull Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull Task task = new Task(userId, name, description);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        return findAll(userId).stream()
                .filter(o -> projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<Task> findTaskByProjectId(@NotNull final String userId, @NotNull String projectId, @Nullable Comparator<Task> comparator) throws SQLException {
        @NotNull final List<Task> taskList = findTaskByProjectId(userId, projectId);
        if (comparator == null) return taskList;
        return taskList.stream()
                .filter(o -> projectId.equals(o.getProjectId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public Task bindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @Nullable final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @Nullable
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET project_id = ? WHERE user_id = ? and id = ?;";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setNull(1, Types.VARCHAR);
            statement.setString(2, userId);
            statement.setString(3, taskId);
            statement.execute();
        }
        return findById(userId, taskId);
    }

}
