package ru.tsc.zherdev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IService;
import ru.tsc.zherdev.tm.exception.empty.EmptyIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyIndexException;
import ru.tsc.zherdev.tm.exception.entity.EmptyComparatorException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IDataConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public void clear() throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public void remove(@Nullable final E entity) throws SQLException {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public void removeById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public void removeByIndex(@Nullable final Integer index) throws SQLException {
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize()) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeByIndex(index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @NotNull
    public List<E> findAll() throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final Comparator<E> comparator) throws SQLException {
        if (comparator == null) throw new EmptyComparatorException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll(comparator);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public E findById(@Nullable final String id) throws SQLException {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public E findByIndex(final Integer index) throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findByIndex(index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public boolean existsById(final String id) throws SQLException {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsById(id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public boolean existsByIndex(final Integer index) throws SQLException {
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.existsByIndex(index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public Integer getSize() throws SQLException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.getSize();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

}
