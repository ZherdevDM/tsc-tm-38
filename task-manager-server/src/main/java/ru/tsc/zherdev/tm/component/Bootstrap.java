package ru.tsc.zherdev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.endpoint.*;
import ru.tsc.zherdev.tm.api.repository.*;
import ru.tsc.zherdev.tm.api.service.*;
import ru.tsc.zherdev.tm.endpoint.ProjectTaskEndpoint;
import ru.tsc.zherdev.tm.endpoint.*;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.*;
import ru.tsc.zherdev.tm.service.*;

import javax.xml.ws.Endpoint;
import java.sql.SQLException;

@Getter
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IServiceLocator serviceLocator = (IServiceLocator) this;
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private  final IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(dataConnectionService.getConnection());
    @NotNull
    private final IOwnerBusinessService<Task> taskService = new TaskService(dataConnectionService, logService);
    @NotNull
    private final IProjectRepository  projectRepository = new ProjectRepository(dataConnectionService.getConnection());
    @NotNull
    private final IOwnerBusinessService<Project> projectService = new ProjectService(dataConnectionService, logService);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(dataConnectionService, logService);
    @NotNull
    private final IUserRepository userRepository = new UserRepository(dataConnectionService.getConnection());
    @NotNull
    private final IUserService userService = new UserService(dataConnectionService, propertyService, logService);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(dataConnectionService.getConnection());
    @NotNull
    private final ISessionService sessionService = new SessionService(logService, dataConnectionService, userService, propertyService);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);
    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);
    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(serviceLocator);

    public Bootstrap() throws SQLException {
    }

    public void start(@NotNull String... args) {
        displayWelcome();
        initEndpoint();
    }

    private void registry(@Nullable final Object endpoint) {
        if (!(endpoint instanceof PublishedEndpoint)) return;
        @NotNull final String protocol = propertyService.getServerProtocol();
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = protocol + "://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }


    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(adminUserEndpoint);
        registry(userEndpoint);
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER SERVER **");
    }

}
