package ru.tsc.zherdev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.dto.Result;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.sql.SQLException;
import java.util.List;

public interface IAdminUserEndpoint extends PublishedEndpoint {

    @WebMethod
    @NotNull User addUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException;

    @WebMethod
    Result addAllUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "collection") @Nullable final List<User> userList
    ) throws SQLException;

    @WebMethod
    Result clearUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result removeByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result removeByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result removeUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException;

    @WebMethod
    @NotNull
    Result  removeUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException;

    @WebMethod
    @NotNull List<User> findAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    @NotNull List<User> findAllUserSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sort") @Nullable final String sort
    ) throws SQLException;

    @WebMethod
    @Nullable User findByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException;

    @WebMethod
    @Nullable User findByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException;

    @WebMethod
    boolean existsByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException;

    @WebMethod
    boolean existsByIndexUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws SQLException;

    @WebMethod
    @Nullable Integer getSizeUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws SQLException;

    @WebMethod
    @Nullable User findUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException;

    @WebMethod
    @Nullable User findUserByEmailUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException;


    @WebMethod
    @Nullable User lockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException;

    @WebMethod
    @Nullable User unLockUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException;

    @WebMethod
    @NotNull User createUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws SQLException;

    @WebMethod
    @NotNull User createUserExtraEmail(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException;

    @WebMethod
    @NotNull User createUserExtraRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException;

    @WebMethod
    @Nullable User updateUser(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "passwordHash") @Nullable final String passwordHash,
            @WebParam(name = "email") @Nullable final String email,
            @WebParam(name = "role") @Nullable final Role role,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "middleName") @Nullable final String middleName
    ) throws SQLException;

    @WebMethod
    @Nullable User setRoleUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "role") @Nullable final Role role
    ) throws SQLException;

    @WebMethod
    @Nullable User findUserByIdUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws SQLException;

    @WebMethod
    boolean isLoginExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws SQLException;

    @WebMethod
    boolean isEmailExistsUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "email") @Nullable final String email
    ) throws SQLException;

}
