package ru.tsc.zherdev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractOwnerBusinessEntity extends AbstractBusinessEntity {

    @Nullable
    protected String userId = null;

}
