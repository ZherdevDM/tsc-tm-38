package ru.tsc.zherdev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.zherdev.tm.api.repository.IOwnerBusinessRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IOwnerBusinessService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.exception.empty.EmptyIdException;
import ru.tsc.zherdev.tm.exception.empty.EmptyIndexException;
import ru.tsc.zherdev.tm.exception.empty.EmptyNameException;
import ru.tsc.zherdev.tm.exception.empty.EmptyStatusException;
import ru.tsc.zherdev.tm.exception.entity.EmptyUserIdException;
import ru.tsc.zherdev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.zherdev.tm.exception.system.IndexIncorrectException;
import ru.tsc.zherdev.tm.exception.system.SQLExecutionFailedException;
import ru.tsc.zherdev.tm.model.AbstractOwnerBusinessEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public abstract class AbstractOwnerBusinessService<E extends AbstractOwnerBusinessEntity>
        implements IOwnerBusinessService<E> {

    @NotNull
    protected IDataConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    protected abstract IOwnerBusinessRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public @NotNull void remove(final @Nullable String userId, final @Nullable E entity) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(entity).orElseThrow(() -> new EntityNotFoundException(entity));
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.remove(userId, entity);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void removeById(final @Nullable String userId, final @Nullable String id) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.removeById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void removeByName(final @Nullable String userId, final @Nullable String name) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void removeByIndex(final @Nullable String userId, final @Nullable Integer index) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public void clear(final @Nullable String userId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public @NotNull Integer getSize(final @Nullable String userId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.getSize(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public boolean existsByIndex(final @Nullable String userId, final @Nullable Integer index) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.existsByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public boolean existsById(final @Nullable String userId, final @Nullable String id) {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.existsById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public @NotNull List<E> findAll(final @Nullable String userId) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public @NotNull List<E> findAll(final @Nullable String userId, final Comparator<E> comparator) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public E findById(final @Nullable String userId, final @Nullable String id) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    public E findByIndex(final @Nullable String userId, final @Nullable Integer index) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public E findByName(final @Nullable String userId, final @Nullable String name) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            return repository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
    }

    @Override
    @Nullable
    public E updateById(final @Nullable String userId, final @Nullable String id, final @Nullable String name, final @Nullable String description) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            entity = repository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E updateByIndex(final @Nullable String userId, final @Nullable Integer index, final @Nullable String name, final @Nullable String description) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            entity = repository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E startByName(final @Nullable String userId, final @Nullable String name) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.startByName(userId, name);
            connection.commit();
            entity = repository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E startById(final @Nullable String userId, final @Nullable String id) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.startById(userId, id);
            connection.commit();
            entity = repository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E startByIndex(final @Nullable String userId, final @Nullable Integer index) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.startByIndex(userId, index);
            connection.commit();
            entity = repository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E finishByName(final @Nullable String userId, final @Nullable String name) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.finishByName(userId, name);
            connection.commit();
            entity = repository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E finishById(final @Nullable String userId, final @Nullable String id) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.finishById(userId, id);
            connection.commit();
            entity = repository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E finishByIndex(final @Nullable String userId, final @Nullable Integer index) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.finishByIndex(userId, index);
            connection.commit();
            entity = repository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E changeStatusByName(final @Nullable String userId, final @Nullable String name, final @Nullable Status status) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.changeStatusByName(userId, name, status);
            connection.commit();
            entity = repository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E changeStatusById(final @Nullable String userId, final @Nullable String id, final @Nullable Status status) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.changeStatusById(userId, id, status);
            connection.commit();
            entity = repository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

    @Override
    @Nullable
    public E changeStatusByIndex(final @Nullable String userId, final @Nullable Integer index, final @Nullable Status status) throws SQLException {
        Optional.ofNullable(userId).orElseThrow(EmptyUserIdException::new);
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index > getSize(userId)) throw new IndexIncorrectException();
        final E entity;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            final IOwnerBusinessRepository<E> repository = getRepository(connection);
            repository.changeStatusByIndex(userId, index, status);
            connection.commit();
            entity = repository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new SQLExecutionFailedException();
        }
        return entity;
    }

}
