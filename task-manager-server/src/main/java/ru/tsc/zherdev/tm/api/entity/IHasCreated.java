package ru.tsc.zherdev.tm.api.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(@NotNull final Date created);

}
