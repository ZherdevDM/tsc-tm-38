package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.TaskRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskServiceTest {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private TaskRepository taskRepository;

    @NotNull
    private TaskService taskService;

    @Nullable
    private Task task;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        taskRepository = new TaskRepository(dataConnectionService.getConnection());
        taskService = new TaskService(dataConnectionService, logService);

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        user.setRole(Role.ADMIN);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        task = taskRepository.add(user.getId(), new Task(user.getId(), "test", "test description"));
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("test", task.getName());
    }

    @Test
    public void create() throws SQLException {
        @Nullable final Task testTask = taskService.create(user.getId(), "Test1", "Test1 description");
        Assert.assertNotNull(testTask);
    }

    @Test
    public void add() throws SQLException {
        @Nullable final Task taskById = taskService.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(user.getId(), "test2", "test2 description"));
        tasks.add(new Task(user.getId(), "test3", "test3 description"));
        taskService.addAll(tasks);
        tasks = taskService.findAll(user.getId());
        Assert.assertNotEquals(Collections.EMPTY_LIST, tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserId() throws SQLException {
        @Nullable List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByName() throws SQLException {
        @Nullable final Task taskByName = taskService.findByName(user.getId(), task.getName());
        Assert.assertNotNull(taskByName);
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Task taskById = taskService.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Task taskByIndex = taskService.findByIndex(user.getId(), 0);
        Assert.assertNotNull(taskByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        int taskSize = taskService.getSize(user.getId());
        Assert.assertEquals(taskSize, tasks.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final Task taskByIndex = taskService.findByIndex(user.getId(), 0);
        Assert.assertNotNull(taskByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final Task taskById = taskService.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskById);
    }

    @Test
    public void updateById() throws SQLException {
        @Nullable final Task updatedTask = taskService.updateById(
                user.getId(), task.getId(), "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("updated test", updatedTask.getName());
        Assert.assertEquals("updated test description", updatedTask.getDescription());
    }

    @Test
    public void updateByIndex() throws SQLException {
        @Nullable final Task updatedTask = taskService.updateByIndex(
                user.getId(), 0, "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("updated test", updatedTask.getName());
        Assert.assertEquals("updated test description", updatedTask.getDescription());
    }

    @Test
    public void changeStatusByName() throws SQLException {
        @Nullable final Task statusChangedTask = taskService.changeStatusByName(user.getId(), task.getName(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void changeStatusById() throws SQLException {
        @Nullable final Task statusChangedTask = taskService.changeStatusById(user.getId(), task.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void changeStatusByIndex() throws SQLException {
        @Nullable final Task statusChangedTask =
                taskService.changeStatusByIndex(user.getId(), 0, Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startByName() throws SQLException {
        @Nullable final Task startedTask = taskService.startByName(user.getId(), task.getName());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startById() throws SQLException {
        @Nullable final Task startedTask = taskService.startById(user.getId(), task.getId());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startByIndex() throws SQLException {
        @Nullable final Task startedTask = taskService.startByIndex(user.getId(), 0);
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishByName() throws SQLException {
        @Nullable final Task finishedTask = taskService.finishByName(user.getId(), task.getName());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishById() throws SQLException {
        @Nullable final Task finishedTask = taskService.finishById(user.getId(), task.getId());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishByIndex() throws SQLException {
        @Nullable final Task finishedTask = taskService.finishByIndex(user.getId(), 0);
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskService.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void clear() throws SQLException {
        @Nullable List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertNotNull(tasks);
        taskService.clear(user.getId());
        Assert.assertEquals(Collections.EMPTY_LIST, taskService.findAll(user.getId()));
        taskService.addAll(tasks);
    }

    @Test
    public void remove() throws SQLException {
        taskService.remove(user.getId(), task);
        Assert.assertNull(taskService.findById(user.getId(), task.getId()));
        taskService.add(user.getId(), task);
    }

    @Test
    public void removeByName() throws SQLException {
        taskService.removeByName(user.getId(), task.getName());
        Assert.assertNull(taskService.findById(user.getId(), task.getId()));
        taskService.add(user.getId(), task);
    }

    @Test
    public void removeById() throws SQLException {
        taskService.removeById(user.getId(), task.getId());
        Assert.assertNull(taskService.findById(user.getId(), task.getId()));
        taskService.add(user.getId(), task);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final Task removableTask = taskService.add(
                user.getId(), new Task(user.getId(), "remove test", "remove test description")
        );
        Assert.assertNotNull(removableTask);
        int tasksSize = taskService.findAll(user.getId()).size();
        Assert.assertNotEquals(0, tasksSize);
        taskService.removeByIndex(user.getId(), tasksSize - 1);
        Assert.assertNull(taskService.findById(user.getId(), removableTask.getId()));
    }

}
