package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.repository.IProjectRepository;
import ru.tsc.zherdev.tm.api.repository.ITaskRepository;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IProjectTaskService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.ProjectRepository;
import ru.tsc.zherdev.tm.repository.TaskRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        projectRepository = new ProjectRepository(dataConnectionService.getConnection());
        taskRepository = new TaskRepository(dataConnectionService.getConnection());
        projectTaskService = new ProjectTaskService(dataConnectionService, logService);

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        user.setRole(Role.ADMIN);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        project = projectRepository.add(user.getId(), new Project(user.getId(), "test PROJECT", "test description"));
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("test PROJECT", project.getName());

        task = taskRepository.create(user.getId(), "test TASK", "DESCRIPTION test");
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("test TASK", task.getName());
        taskRepository.add(user.getId(), task);
        Assert.assertNotNull(taskRepository.findById(user.getId(), task.getId()));
        @Nullable final Task testTask = projectTaskService.bindTaskById(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(testTask);
    }

    @Test
    public void findTaskByProjectId() throws SQLException {
        @Nullable List<Task> testTasks = projectTaskService.findTaskByProjectId(user.getId(), project.getId());
        Assert.assertNotNull(testTasks);
        @Nullable List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        Assert.assertEquals(tasks, testTasks);
    }

    @Test
    public void bindTaskById() throws SQLException {
        @Nullable final Task testTask = projectTaskService.bindTaskById(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(testTask.getProjectId());
    }

    @Test
    public void unbindTaskById() throws SQLException {
        @Nullable final Task testTask = projectTaskService.unbindTaskById(user.getId(), project.getId(), task.getId());
        Assert.assertNull(testTask.getProjectId());
    }

    @Test
    public void removeProjectById() throws SQLException {
        projectTaskService.removeProjectById(user.getId(), project.getId());
        Assert.assertNotNull(taskRepository.findById(user.getId(), task.getId()));
        Assert.assertNull(projectRepository.findById(user.getId(), project.getId()));
    }

}
