package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private UserRepository userRepository;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        userRepository = new UserRepository(dataConnectionService.getConnection());

        user = userRepository.add("login", "passwordHash");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());
    }

    @Test
    public void add() throws SQLException {
        @Nullable final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull final List<User> users = new ArrayList<>();
        users.add(user);
        users.add(new User("test2", "pass2", Role.USER));
        users.add(new User("test3", "pass3", Role.ADMIN));
        userRepository.remove(user);
        userRepository.addAll(users);
        Assert.assertEquals(users, userRepository.findAll());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void clear() throws SQLException {
        @Nullable List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        userRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, userRepository.findAll());
        userRepository.addAll(users);
    }

    @Test
    public void remove() throws SQLException {
        userRepository.remove(user);
        Assert.assertNull(userRepository.findById(user.getId()));
        userRepository.add(user);
    }

    @Test
    public void removeById() throws SQLException {
        userRepository.removeById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
        userRepository.add(user);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final User removableUser = userRepository.add(
                new User("remove test", "remove test description")
        );
        Assert.assertNotNull(removableUser);
        int usersSize = userRepository.findAll().size();
        Assert.assertNotEquals(0, usersSize);
        userRepository.removeByIndex(usersSize - 1);
        Assert.assertNull(userRepository.findById(removableUser.getId()));
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final User userByIndex = userRepository.findByIndex(0);
        Assert.assertNotNull(userByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<User> users = userRepository.findAll();
        Assert.assertNotNull(users);
        int userSize = userRepository.getSize();
        Assert.assertEquals(userSize, users.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final User userByIndex = userRepository.findByIndex(0);
        Assert.assertNotNull(userByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
    }

    @Test
    public void create() throws SQLException {
        @Nullable final User testUser = userRepository.add("Test1", "Test1 description");
        Assert.assertNotNull(testUser);
    }

    @Test
    public void findUserByLogin() throws SQLException {
        @Nullable User userByLogin = userRepository.findUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(user, userByLogin);
    }

    @Test
    public void findUserByEmail() throws SQLException {
        @Nullable User userByEmail = userRepository.findUserByEmail(user.getEmail());
        Assert.assertNotNull(userByEmail);
        Assert.assertEquals(user, userByEmail);
    }

    @Test
    public void removeUserById() throws SQLException {
        userRepository.removeUserById(user.getId());
        Assert.assertNull(userRepository.findById(user.getId()));
        userRepository.add(user);
    }

    @Test
    public void removeUserByLogin() throws SQLException {
        userRepository.removeUserByLogin(user.getLogin());
        Assert.assertNull(userRepository.findUserByLogin(user.getLogin()));
        userRepository.add(user);
    }

    @Test
    public void update() throws SQLException {
        @NotNull final String email = "email@g.com";
        @NotNull final String lastName = "Smith";
        @NotNull final String firstName = "John";
        @NotNull final String middleName = "Billy";
        @Nullable User updatedUser = userRepository.update(user.getId(), user.getLogin(),
                user.getPasswordHash(), email, Role.ADMIN, lastName, firstName, middleName);
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(user.getId(), updatedUser.getId());
        Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), updatedUser.getPasswordHash());
        Assert.assertEquals(email, updatedUser.getEmail());
        Assert.assertEquals(Role.ADMIN, updatedUser.getRole());
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test
    public void lockUserByLogin() throws SQLException {
        @Nullable User userByLogin = userRepository.lockUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(true, userByLogin.getLocked());
        userRepository.unLockUserByLogin(user.getLogin());
    }

    @Test
    public void unLockUserByLogin() throws SQLException {
        @Nullable User userByLogin = userRepository.unLockUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(false, userByLogin.getLocked());
    }

}
