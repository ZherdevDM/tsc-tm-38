package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.UserRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private Connection connection;

    @NotNull
    private UserService userService;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        dataConnectionService = new DataConnectionService(propertyService, logService);
        connection = dataConnectionService.getConnection();
        @NotNull UserRepository userRepository = new UserRepository(connection);
        @NotNull final PropertyService propertyService = new PropertyService();
        userService = new UserService(dataConnectionService, propertyService, logService);

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        user.setRole(Role.ADMIN);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        user = userRepository.add(new User("test", "passwordHash", Role.ADMIN));
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("test", user.getLogin());
    }

    @Test
    public void create() throws SQLException {
        @Nullable final User testUser = userService.create(user.getId(), "Test1", "Test1 description");
        Assert.assertNotNull(testUser);
    }

    @Test
    public void add() throws SQLException {
        @Nullable final User userById = userService.findById(user.getId());
        connection.commit();
        Assert.assertNotNull(userById);
        Assert.assertEquals(user, userById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<User> users = new ArrayList<>();
        users.add(new User("test2", "test2", Role.USER));
        users.add(new User("test2", "test3"));
        userService.addAll(users);
        users = userService.findAll();
        connection.commit();
        Assert.assertNotEquals(Collections.EMPTY_LIST, users);
        Assert.assertEquals(3, users.size());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findAllByUserId() throws SQLException {
        @Nullable List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final User userByIndex = userService.findByIndex(0);
        Assert.assertNotNull(userByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        int userSize = userService.getSize();
        Assert.assertEquals(userSize, users.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final User userByIndex = userService.findByIndex(0);
        Assert.assertNotNull(userByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
    }


    @Test
    public void remove() throws SQLException {
        userService.remove(user);
        Assert.assertNull(userService.findById(user.getId()));
        userService.add(user);
    }

    @Test
    public void removeById() throws SQLException {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
        userService.add(user);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final User removableUser = userService.add(new User("remove test", "pass"));
        Assert.assertNotNull(removableUser);
        int usersSize = userService.findAll().size();
        Assert.assertNotEquals(0, usersSize);
        userService.removeByIndex(usersSize - 1);
        Assert.assertNull(userService.findById(removableUser.getId()));
    }

    @Test
    public void update() throws SQLException {
        @NotNull final String email = "email@g.com";
        @NotNull final String lastName = "Smith";
        @NotNull final String firstName = "John";
        @NotNull final String middleName = "Billy";
        @Nullable User updatedUser = userService.update(user.getId(), user.getLogin(),
                user.getPasswordHash(), email, Role.ADMIN, lastName, firstName, middleName);
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(user.getId(), updatedUser.getId());
        Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), updatedUser.getPasswordHash());
        Assert.assertEquals(email, updatedUser.getEmail());
        Assert.assertEquals(Role.ADMIN, updatedUser.getRole());
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test
    public void lockUserByLogin() throws SQLException {
        @Nullable User userByLogin = userService.lockUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(true, userByLogin.getLocked());
        userService.unLockUserByLogin(user.getLogin());
    }

    @Test
    public void unLockUserByLogin() throws SQLException {
        @Nullable User userByLogin = userService.unLockUserByLogin(user.getLogin());
        Assert.assertNotNull(userByLogin);
        Assert.assertEquals(false, userByLogin.getLocked());
    }

}
