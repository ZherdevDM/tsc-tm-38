package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private SessionRepository sessionRepository;

    @Nullable
    private Session session;

    @Nullable User user;

    @Before
    public void before() throws SQLException {
        sessionRepository = new SessionRepository(dataConnectionService.getConnection());

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        session = new Session();
        session.setUserId(user.getId());
        Assert.assertNotNull(session.getId());
    }

    @Test
    public void add() throws SQLException {
        @Nullable final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull final List<Session> sessions = new ArrayList<>();
        sessions.add(session);
        sessions.add(new Session());
        sessions.add(new Session());
        sessionRepository.remove(session);
        sessionRepository.addAll(sessions);
        Assert.assertEquals(sessions, sessionRepository.findAll());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<Session> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(1, sessions.size());
    }

    @Test
    public void clear() throws SQLException {
        @Nullable List<Session> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        sessionRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, sessionRepository.findAll());
        sessionRepository.addAll(sessions);
    }

    @Test
    public void remove() throws SQLException {
        sessionRepository.remove(session);
        Assert.assertNull(sessionRepository.findById(session.getId()));
        sessionRepository.open(session);
    }

    @Test
    public void removeById() throws SQLException {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
        sessionRepository.open(session);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final Session removableUser = sessionRepository.open(new Session());
        Assert.assertNotNull(removableUser);
        int sessionsSize = sessionRepository.findAll().size();
        Assert.assertNotEquals(0, sessionsSize);
        sessionRepository.removeByIndex(sessionsSize - 1);
        Assert.assertNull(sessionRepository.findById(removableUser.getId()));
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Session sessionByIndex = sessionRepository.findByIndex(0);
        Assert.assertNotNull(sessionByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<Session> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        int sessionSize = sessionRepository.getSize();
        Assert.assertEquals(sessionSize, sessions.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final Session sessionByIndex = sessionRepository.findByIndex(0);
        Assert.assertNotNull(sessionByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
    }

    @Test
    public void open() throws SQLException {
        sessionRepository.open(session);
        @Nullable Session openedSession = sessionRepository.findById(session.getId());
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(session.getId(), openedSession.getId());
    }

    @Test
    public void contains() throws SQLException {
        @Nullable Session nonContainedSession = new Session();
        Assert.assertNotNull(nonContainedSession);
        boolean isContains = sessionRepository.contains(nonContainedSession.getId());
        Assert.assertEquals(false, isContains);
        isContains = sessionRepository.contains(session.getId());
        Assert.assertEquals(true, isContains);
    }

    @Test
    public void removeByUserId() throws SQLException {
        @Nullable boolean isRemoved = sessionRepository.removeByUserId(session.getUserId());
        Assert.assertTrue(isRemoved);
        @Nullable Session removedSession = sessionRepository.findById(session.getId());
        Assert.assertNull(removedSession);
    }

    @Test
    public void close() throws SQLException {
        @Nullable final Session closableSession = new Session();
        Assert.assertNotNull(closableSession);
        @Nullable final Session openedSession = sessionRepository.open(closableSession);
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(closableSession.getId(), openedSession.getId());
        boolean isClosed = sessionRepository.close(closableSession);
        Assert.assertEquals(true, isClosed);
    }

}
