package ru.tsc.zherdev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Role;
import ru.tsc.zherdev.tm.exception.user.AccessDeniedException;
import ru.tsc.zherdev.tm.model.Session;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.repository.SessionRepository;
import ru.tsc.zherdev.tm.repository.UserRepository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private SessionService sessionService;

    @NotNull
    private UserRepository userRepository;

    @NotNull
    private Session session;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        @NotNull final ILogService logService = new LogService();
        @NotNull final PropertyService propertyService = new PropertyService();
        @NotNull final DataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);
        userRepository = new UserRepository(dataConnectionService.getConnection());
        @NotNull final SessionRepository sessionRepository = new SessionRepository(dataConnectionService.getConnection());
        @NotNull final UserService userService = new UserService(dataConnectionService, propertyService, logService);
        sessionService = new SessionService(logService, dataConnectionService, userService, propertyService);

        user = new User("login", "dd554650e5558cbe375c6bf958741161", Role.ADMIN);
        Assert.assertNotNull(user);
        userRepository.add(user);
        user = userRepository.findUserByLogin(user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        session = new Session();
        session.setUserId(user.getId());
        Assert.assertNotNull(session);
        sessionService.sign(session);
        Assert.assertNotNull(session.getId());
        sessionService.open(session);
    }

    @Test
    public void add() throws SQLException {
        sessionService.open(session);
        @Nullable final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<Session> sessions = new ArrayList<>();
        sessions.add(new Session());
        sessions.add(new Session());
        sessions.forEach(s -> {
            sessionService.open(s);
        });
        sessions = sessionService.findAll();
        Assert.assertNotEquals(Collections.EMPTY_LIST, sessions);
        Assert.assertEquals(3, sessions.size());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(1, sessions.size());
    }

    @Test
    public void findAllBySessionId() throws SQLException {
        @Nullable List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertEquals(1, sessions.size());
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Session sessionByIndex = sessionService.findByIndex(0);
        Assert.assertNotNull(sessionByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<Session> sessions = sessionService.findAll();
        Assert.assertNotNull(sessions);
        int sessionSize = sessionService.getSize();
        Assert.assertEquals(sessionSize, sessions.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final Session sessionByIndex = sessionService.findByIndex(0);
        Assert.assertNotNull(sessionByIndex);
    }

    @Test
    public void remove() throws SQLException {
        sessionService.remove(session);
        Assert.assertNull(sessionService.findById(session.getId()));
        sessionService.open(session);
    }

    @Test
    public void removeById() throws SQLException {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
        sessionService.open(session);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final Session removableSession = sessionService.open(new Session());
        Assert.assertNotNull(removableSession);
        int sessionsSize = sessionService.findAll().size();
        Assert.assertNotEquals(0, sessionsSize);
        sessionService.removeByIndex(sessionsSize - 1);
        Assert.assertNull(sessionService.findById(removableSession.getId()));
    }


    @Test
    public void open() throws SQLException {
        sessionService.open(session);
        @Nullable Session openedSession = sessionService.findById(session.getId());
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(session.getId(), openedSession.getId());
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable Session nonContainedSession = new Session();
        Assert.assertNotNull(nonContainedSession);
        boolean isExists = sessionService.existsById(nonContainedSession.getId());
        Assert.assertFalse(isExists);
        isExists = sessionService.existsById(session.getId());
        Assert.assertTrue(isExists);
    }

    @Test
    public void close() throws SQLException {
        @NotNull final User newUser = createNewUser();
        @Nullable final Session closableSession = createNewSession(newUser);
        Assert.assertNotNull(closableSession);
        @Nullable final Session openedSession = sessionService.open(closableSession);
        Assert.assertNotNull(openedSession);
        Assert.assertEquals(closableSession.getId(), openedSession.getId());
        sessionService.close(closableSession);
        Assert.assertNull(sessionService.findById(closableSession.getId()));
        removeUserAndSession(newUser, null);
    }

    @Test
    public void checkDataAccess() throws SQLException {
        boolean isValid = sessionService.checkDataAccess(user.getLogin(), "admin");
        Assert.assertTrue(isValid);
        isValid = sessionService.checkDataAccess("wrong login", "Not a passwordHash");
        Assert.assertFalse(isValid);
    }

    @Test
    public void login() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signAndValidate() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        sessionService.sign(newSession);
        try {
            sessionService.validate(newSession);
        } catch (AccessDeniedException e) {
            Assert.fail();
        }

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void getUser() throws SQLException {
        @Nullable final User accessedUser = sessionService.getUser(session);
        Assert.assertNotNull(accessedUser);
        Assert.assertEquals(user.getId(), accessedUser.getId());
    }

    @Test
    public void getUserId() throws SQLException {
        @Nullable final String accessedUserId = sessionService.getUserId(session);
        Assert.assertNotNull(accessedUserId);
        Assert.assertEquals(user.getId(), accessedUserId);
    }

    @Test
    public void getListSession() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        @Nullable List<Session> sessions = sessionService.getListSession(newSession);
        Assert.assertNotNull(sessions);

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void closeAll() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        Assert.assertNotEquals(Collections.EMPTY_LIST, sessionService.findAll());
        sessionService.closeAll(session);
        Assert.assertEquals(Collections.EMPTY_LIST, sessionService.findAll());

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void validateAdmin() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        try {
            sessionService.validateAdmin(newSession);
        } catch (AccessDeniedException | SQLException e) {
            Assert.fail();
        }
        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void isValid() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        boolean isValid = sessionService.isValid(newSession);
        Assert.assertTrue(isValid);
        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signOutByLogin() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);

        sessionService.signOutByLogin(newUser.getLogin());
        Assert.assertNull(sessionService.findById(newSession.getId()));

        removeUserAndSession(newUser, newSession);
    }

    @Test
    public void signOutByUserId() throws SQLException {
        @Nullable final User newUser = createNewUser();
        @Nullable final Session newSession = createNewSession(newUser);
        sessionService.signOutByUserId(newUser.getId());
        Assert.assertNull(sessionService.findById(newSession.getId()));

        removeUserAndSession(newUser, newSession);

    }

    @NotNull
    private User createNewUser() throws SQLException {
        @Nullable User newUser = userRepository.add("user", "78a71b2088b00afe56d5b90e22f805fa");
        Assert.assertNotNull(newUser);
        newUser.setRole(Role.ADMIN);
        userRepository.add(newUser);
        return newUser;
    }

    @NotNull
    private Session createNewSession(@NotNull final User newUser) throws SQLException {
        @Nullable Session newSession = sessionService.login(newUser.getLogin(), "user");
        Assert.assertNotNull(newSession);
        Assert.assertNotNull(sessionService.findById(newSession.getId()));
        return newSession;
    }

    private void removeUserAndSession(@Nullable final User newUser, @Nullable final Session newSession) throws SQLException {
        if (user != null) userRepository.remove(newUser);
        if (newSession != null) sessionService.remove(newSession);
    }

}
