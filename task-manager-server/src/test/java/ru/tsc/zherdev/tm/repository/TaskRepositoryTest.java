package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.Task;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @NotNull
    private TaskRepository taskRepository;

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        taskRepository = new TaskRepository(dataConnectionService.getConnection());

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        project = new Project(user.getId(), "test", "test description");
        project.setName("project");
        project.setDescription("project description");
        project.setUserId(user.getId());
        Assert.assertNotNull(project);


        task = taskRepository.add(user.getId(), new Task(user.getId(), "test", "test description"));
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("test", task.getName());
        taskRepository.bindTaskById(user.getId(), project.getId(), task.getId());
    }

    @Test
    public void create() throws SQLException {
        @Nullable final Task testTask = taskRepository.create(user.getId(), "Test1", "Test1 description");
        Assert.assertNotNull(testTask);
    }

    @Test
    public void add() throws SQLException {
        @Nullable final Task taskById = taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(user.getId(), "test2", "test2 description"));
        tasks.add(new Task(user.getId(), "test3", "test3 description"));
        taskRepository.addAll(tasks);
        tasks = taskRepository.findAll();
        Assert.assertNotEquals(Collections.EMPTY_LIST, tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserId() throws SQLException {
        @Nullable List<Task> tasks = taskRepository.findAll(user.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByName() throws SQLException {
        @Nullable final Task taskByName = taskRepository.findByName(user.getId(), task.getName());
        Assert.assertNotNull(taskByName);
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Task taskById = taskRepository.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Task taskByIndex = taskRepository.findByIndex(user.getId(), 0);
        Assert.assertNotNull(taskByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        int taskSize = taskRepository.getSize();
        Assert.assertEquals(taskSize, tasks.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final Task taskByIndex = taskRepository.findByIndex(user.getId(), 0);
        Assert.assertNotNull(taskByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final Task taskById = taskRepository.findById(user.getId(), task.getId());
        Assert.assertNotNull(taskById);
    }

    @Test
    public void updateById() throws SQLException {
        @Nullable final Task updatedTask = taskRepository.updateById(
                user.getId(), task.getId(), "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("updated test", updatedTask.getName());
        Assert.assertEquals("updated test description", updatedTask.getDescription());
    }

    @Test
    public void updateByIndex() throws SQLException {
        @Nullable final Task updatedTask = taskRepository.updateByIndex(
                user.getId(), 0, "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals("updated test", updatedTask.getName());
        Assert.assertEquals("updated test description", updatedTask.getDescription());
    }

    @Test
    public void changeStatusByName() throws SQLException {
        @Nullable final Task statusChangedTask = taskRepository.changeStatusByName(user.getId(), task.getName(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void changeStatusById() throws SQLException {
        @Nullable final Task statusChangedTask = taskRepository.changeStatusById(user.getId(), task.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void changeStatusByIndex() throws SQLException {
        @Nullable final Task statusChangedTask =
                taskRepository.changeStatusByIndex(user.getId(), 0, Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedTask);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startByName() throws SQLException {
        @Nullable final Task startedTask = taskRepository.startByName(user.getId(), task.getName());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startById() throws SQLException {
        @Nullable final Task startedTask = taskRepository.startById(user.getId(), task.getId());
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void startByIndex() throws SQLException {
        @Nullable final Task startedTask = taskRepository.startByIndex(user.getId(), 0);
        Assert.assertNotNull(startedTask);
        Assert.assertEquals(Status.IN_PROGRESS, startedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishByName() throws SQLException {
        @Nullable final Task finishedTask = taskRepository.finishByName(user.getId(), task.getName());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishById() throws SQLException {
        @Nullable final Task finishedTask = taskRepository.finishById(user.getId(), task.getId());
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void finishByIndex() throws SQLException {
        @Nullable final Task finishedTask = taskRepository.finishByIndex(user.getId(), 0);
        Assert.assertNotNull(finishedTask);
        Assert.assertEquals(Status.COMPLETE, finishedTask.getStatus());
        taskRepository.changeStatusByName(user.getId(), task.getName(), task.getStatus());
    }

    @Test
    public void clear() throws SQLException {
        @Nullable List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
        taskRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAll());
        taskRepository.addAll(tasks);
    }

    @Test
    public void remove() throws SQLException {
        taskRepository.remove(user.getId(), task);
        Assert.assertNull(taskRepository.findById(user.getId(), task.getId()));
        taskRepository.add(user.getId(), task);
    }

    @Test
    public void removeByName() throws SQLException {
        taskRepository.removeByName(user.getId(), task.getName());
        Assert.assertNull(taskRepository.findById(user.getId(), task.getId()));
        taskRepository.add(user.getId(), task);
    }

    @Test
    public void removeById() throws SQLException {
        taskRepository.removeById(user.getId(), task.getId());
        Assert.assertNull(taskRepository.findById(user.getId(), task.getId()));
        taskRepository.add(user.getId(), task);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final Task removableTask = taskRepository.add(
                user.getId(), new Task(user.getId(), "remove test", "remove test description")
        );
        Assert.assertNotNull(removableTask);
        int tasksSize = taskRepository.findAll(user.getId()).size();
        Assert.assertNotEquals(0, tasksSize);
        taskRepository.removeByIndex(user.getId(), tasksSize - 1);
        Assert.assertNull(taskRepository.findById(user.getId(), removableTask.getId()));
    }

    @Test
    public void findTaskByProjectId() throws SQLException {
        @Nullable final List<Task> tasks = taskRepository.findTaskByProjectId(user.getId(), project.getId());
        Assert.assertNotNull(tasks);
        for (Task t : tasks) {
            Assert.assertEquals(project.getId(), t.getProjectId());
        }
    }

    @Test
    public void unbindTaskById() throws SQLException {
        taskRepository.unbindTaskById(user.getId(), project.getId(), task.getId());
        @Nullable final Task testTask = taskRepository.findById(user.getId(), task.getId());
        Assert.assertNotNull(testTask);
        Assert.assertNull(testTask.getProjectId());
    }

    @Test
    public void bindTaskById() throws SQLException {
        taskRepository.bindTaskById(user.getId(), project.getId(), task.getId());
        @Nullable final Task testTask = taskRepository.findById(user.getId(), task.getId());
        Assert.assertNotNull(testTask);
        Assert.assertEquals(project.getId(), testTask.getProjectId());
    }

}
