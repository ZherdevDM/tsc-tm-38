package ru.tsc.zherdev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.zherdev.tm.api.service.IDataConnectionService;
import ru.tsc.zherdev.tm.api.service.ILogService;
import ru.tsc.zherdev.tm.api.service.IPropertyService;
import ru.tsc.zherdev.tm.enumerated.Status;
import ru.tsc.zherdev.tm.model.Project;
import ru.tsc.zherdev.tm.model.User;
import ru.tsc.zherdev.tm.service.DataConnectionService;
import ru.tsc.zherdev.tm.service.LogService;
import ru.tsc.zherdev.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private ProjectRepository projectRepository;

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @NotNull
    private ILogService logService = new LogService();

    @NotNull
    private IDataConnectionService dataConnectionService = new DataConnectionService(propertyService, logService);

    @Nullable
    private Project project;

    @Nullable
    private User user;

    @Before
    public void before() throws SQLException {
        projectRepository = new ProjectRepository(dataConnectionService.getConnection());

        user = new User("login", "passwordHash");
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("login", user.getLogin());

        project = projectRepository.add(user.getId(), new Project(user.getId(), "test", "test description"));
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("test", project.getName());
    }

    @Test
    public void create() throws SQLException {
        @Nullable final Project testProject = projectRepository.create(user.getId(), "Test1", "Test1 description");
        Assert.assertNotNull(testProject);
    }

    @Test
    public void add() throws SQLException {
        @Nullable final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project, projectById);
    }

    @Test
    public void addAll() throws SQLException {
        @NotNull List<Project> projects = new ArrayList<>();
        projects.add(new Project(user.getId(), "test2", "test2 description"));
        projects.add(new Project(user.getId(), "test3", "test3 description"));
        projectRepository.addAll(projects);
        projects = projectRepository.findAll();
        Assert.assertNotEquals(Collections.EMPTY_LIST, projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findAll() throws SQLException {
        @Nullable List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() throws SQLException {
        @Nullable List<Project> projects = projectRepository.findAll(user.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findByName() throws SQLException {
        @Nullable final Project projectByName = projectRepository.findByName(user.getId(), project.getName());
        Assert.assertNotNull(projectByName);
    }

    @Test
    public void findById() throws SQLException {
        @Nullable final Project projectById = projectRepository.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findByIndex() throws SQLException {
        @Nullable final Project projectByIndex = projectRepository.findByIndex(user.getId(), 0);
        Assert.assertNotNull(projectByIndex);
    }

    @Test
    public void getSize() throws SQLException {
        @Nullable List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        int projectSize = projectRepository.getSize();
        Assert.assertEquals(projectSize, projects.size());
    }

    @Test
    public void existsByIndex() throws SQLException {
        @Nullable final Project projectByIndex = projectRepository.findByIndex(user.getId(), 0);
        Assert.assertNotNull(projectByIndex);
    }

    @Test
    public void existsById() throws SQLException {
        @Nullable final Project projectById = projectRepository.findById(user.getId(), project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void updateById() throws SQLException {
        @Nullable final Project updatedProject = projectRepository.updateById(
                user.getId(), project.getId(), "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("updated test", updatedProject.getName());
        Assert.assertEquals("updated test description", updatedProject.getDescription());
    }

    @Test
    public void updateByIndex() throws SQLException {
        @Nullable final Project updatedProject = projectRepository.updateByIndex(
                user.getId(), 0, "updated test", "updated test description"
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals("updated test", updatedProject.getName());
        Assert.assertEquals("updated test description", updatedProject.getDescription());
    }

    @Test
    public void changeStatusByName() throws SQLException {
        @Nullable final Project statusChangedProject = projectRepository.changeStatusByName(user.getId(), project.getName(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void changeStatusById() throws SQLException {
        @Nullable final Project statusChangedProject = projectRepository.changeStatusById(user.getId(), project.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void changeStatusByIndex() throws SQLException {
        @Nullable final Project statusChangedProject =
                projectRepository.changeStatusByIndex(user.getId(), 0, Status.IN_PROGRESS);
        Assert.assertNotNull(statusChangedProject);
        Assert.assertEquals(Status.IN_PROGRESS, statusChangedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startByName() throws SQLException {
        @Nullable final Project startedProject = projectRepository.startByName(user.getId(), project.getName());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startById() throws SQLException {
        @Nullable final Project startedProject = projectRepository.startById(user.getId(), project.getId());
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void startByIndex() throws SQLException {
        @Nullable final Project startedProject = projectRepository.startByIndex(user.getId(), 0);
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS, startedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishByName() throws SQLException {
        @Nullable final Project finishedProject = projectRepository.finishByName(user.getId(), project.getName());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishById() throws SQLException {
        @Nullable final Project finishedProject = projectRepository.finishById(user.getId(), project.getId());
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void finishByIndex() throws SQLException {
        @Nullable final Project finishedProject = projectRepository.finishByIndex(user.getId(), 0);
        Assert.assertNotNull(finishedProject);
        Assert.assertEquals(Status.COMPLETE, finishedProject.getStatus());
        projectRepository.changeStatusByName(user.getId(), project.getName(), project.getStatus());
    }

    @Test
    public void clear() throws SQLException {
        @Nullable List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        projectRepository.clear();
        Assert.assertEquals(Collections.EMPTY_LIST, projectRepository.findAll());
        projectRepository.addAll(projects);
    }

    @Test
    public void remove() throws SQLException {
        projectRepository.remove(user.getId(), project);
        Assert.assertNull(projectRepository.findById(user.getId(), project.getId()));
        projectRepository.add(user.getId(), project);
    }

    @Test
    public void removeByName() throws SQLException {
        projectRepository.removeByName(user.getId(), project.getName());
        Assert.assertNull(projectRepository.findById(user.getId(), project.getId()));
        projectRepository.add(user.getId(), project);
    }

    @Test
    public void removeById() throws SQLException {
        projectRepository.removeById(user.getId(), project.getId());
        Assert.assertNull(projectRepository.findById(user.getId(), project.getId()));
        projectRepository.add(user.getId(), project);
    }

    @Test
    public void removeByIndex() throws SQLException {
        @Nullable final Project removableProject = projectRepository.add(
                user.getId(), new Project(user.getId(), "remove test", "remove test description")
        );
        Assert.assertNotNull(removableProject);
        int projectsSize = projectRepository.findAll(user.getId()).size();
        Assert.assertNotEquals(0, projectsSize);
        projectRepository.removeByIndex(user.getId(), projectsSize - 1);
        Assert.assertNull(projectRepository.findById(user.getId(), removableProject.getId()));
    }

}